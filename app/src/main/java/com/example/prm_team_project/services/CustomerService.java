package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.Customer;
import com.example.prm_team_project.models.requests.NewCustomerRequest;
import com.example.prm_team_project.models.requests.SearchCustomerRequest;
import com.example.prm_team_project.models.requests.UpdateCustomerRequest;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Optional;

@RequiresApi(api = 33)
public class CustomerService extends BaseService  {
    
    public List<Customer> findCustomers(SearchCustomerRequest request) {
        return Lists.newArrayList();
    
//        return customerRepository.findAll();
    }
    
    public Customer getCustomerById(Long id) {
        return customerRepository.findOneOrThrow(id, "Customer not found");
    }
    
    public Optional<Customer> getByUserId(Long id) {
        return customerRepository.findOneByField("userId", id);
    }
    
    public List<Customer> getAllCustomerOfUserWithId(Long id) {
        return customerRepository.findAllByField("userId", id);
    }
    
    public Customer addCustomer(NewCustomerRequest request) {
        validate(request);
        Customer customer = request.toCustomer();
        return customerRepository.save(customer);
    }
    
    public Customer updateCustomer(UpdateCustomerRequest request) {
        validate(request);
        Customer customer = customerRepository.findOneOrThrow(request.getCustomerId(), "Customer not found");
        Customer updateCustomer = request.updateFor(customer);
        return customerRepository.save(updateCustomer);
    }
    
    public void deleteCustomer(Long id) {
        customerRepository.delete(id);
    }
}
