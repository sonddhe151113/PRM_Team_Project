package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.models.requests.NewAdminUserRequest;
import com.example.prm_team_project.services.mail.NewAdminUserMailService;
import com.example.prm_team_project.utils.ApiStrings;

@RequiresApi(api = 33)
public class AdminService extends BaseService {
    public void updateUserStatus(Long userId, User.Status status) {
        User user = userRepository.findOneOrThrow(userId, "User not found");
        user.setStatus(status);
        userRepository.save(user);
    }
    
    public User createNewUser(NewAdminUserRequest request) {
        checkSyncData();
        validate(request);
        User user = request.toUser();
    
        String password = ApiStrings.generateRandomStringWithLength(12);
        String encryptedPassword = ApiStrings.getEncryptedPassword(password);
        user.setPassword(encryptedPassword);
        User savedUser = userRepository.save(user);
        
        new NewAdminUserMailService().setUsername(user.getUsername()).setPassword(password).sendTo(user.getEmail());
        System.out.println(String.format("Sign up successfully with email: %s and password: %s ", request.getEmail(), password));
        
        return savedUser;
    }
}
