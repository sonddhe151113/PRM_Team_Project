package com.example.prm_team_project.services;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.Customer;
import com.example.prm_team_project.entities.Order;
import com.example.prm_team_project.entities.OrderDetail;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.models.dtos.ApiException;
import com.example.prm_team_project.models.dtos.Cart;
import com.example.prm_team_project.models.dtos.CartItem;
import com.example.prm_team_project.models.responses.CartDetailResponse;
import com.example.prm_team_project.models.responses.CartDetailResponse.CartProductResponse;
import com.example.prm_team_project.utils.ApiCollections;
import com.example.prm_team_project.utils.ApiMoments;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@RequiresApi(api = Build.VERSION_CODES.O)
public class OrderService  extends BaseService {
    
    public List<Order> getAllOrders() {
        return Lists.newArrayList();
    
//        return orderRepository.findAll();
    }
    
    public List<Order> getAllOfCustomer(Long customerId) {
        return Lists.newArrayList();
    
//        return orderRepository.findAll().stream().filter(x -> Objects.equals(x.getCustomerId(), customerId)).collect(Collectors.toList());
    }
    
    public Order getOne(Long orderId) {
        return orderRepository.findOne(orderId).orElseThrow(() -> ApiException.withMessage("Order not found"));
    }
    
    public void addToCart(Long productId, int total, String note) {
        Cart cart = getCart();
        cart.add(productId, total, note);
        saveCart(cart);
    }
    
    public CartDetailResponse getCartDetail() {
        double totalPrice = getTotalPriceOfCart();
        List<CartProductResponse> items = getCartProductResponses();
    
        return CartDetailResponse.builder().items(items).totalPrice(totalPrice).build();
    }
    
    private double getTotalPriceOfCart() {
        Cart cart = getCart();
        
        Map<Long, CartItem> cartItemMap = ApiCollections.mapOf(cart.getAll(), CartItem::getProductId);
        List<Product> products = productRepository.findAll(cartItemMap.keySet());
        return products.stream().map(product -> {
            CartItem cartItem = cartItemMap.get(product.getId());
            return Objects.requireNonNull(cartItem).getTotal() * product.getUnitPrice();
        }).mapToDouble(x -> x).sum();
    }
    
    @NotNull
    private List<CartProductResponse> getCartProductResponses() {
        Cart cart = getCart();
        
        Map<Long, CartItem> cartItemMap = ApiCollections.mapOf(cart.getAll(), CartItem::getProductId);
        List<Product> products = productRepository.findAll(cartItemMap.keySet());
        
        return products.stream().map(product -> {
            CartItem cartItem = cartItemMap.get(product.getId());
            return CartProductResponse.of(product, Objects.requireNonNull(cartItem).getNote(), cartItem.getTotal());
        }).collect(Collectors.toList());
    }
    
    public void removeFromCart(Long productId) {
        Cart cart = getCart();
        cart.remove(productId);
        saveCart(cart);
    }
    
    public void purchase(List<Long> productIds) {
        Order order = saveOrder();
        
        List<CartProductResponse> products = getCartProductResponses();
        List<OrderDetail> orderDetails = products.stream().map(p -> mapProductResponseToOrderDetail(order, p)).collect(Collectors.toList());
        
        orderDetailRepository.save(orderDetails);
        
        Cart cart = getCart();
        cart.remove(productIds);
        saveCart(cart);
    }
    
    public void purchase(Long productId) {
        purchase(Lists.newArrayList(productId));
    }
    
    public void purchaseAll() {
        Cart cart = getCart();
        List<Long> allIds = cart.getAll().stream().map(CartItem::getProductId).collect(Collectors.toList());
        purchase(allIds);
    }
    
    private OrderDetail mapProductResponseToOrderDetail(final Order order, final CartProductResponse p) {
        return OrderDetail.builder().orderId(order.getId()).productId(p.getId()).unitPrice(p.getUnitPrice()).quantity(p.getQuantity()).discount(0F).build();
    }
    
    private Order saveOrder() {
        User user = AuthService.getCurrentUser();
    
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime requiredDate =  LocalDateTime.now().plusDays(1);
        
        Customer customer = customerRepository.getOneByUserId(user.getId());
        
        Order order = Order.builder()
            .orderDate(ApiMoments.timestampOf(now))
            .customerId(customer.getId())
            .address(customer.getAddress())
            .requiredDate(ApiMoments.timestampOf(requiredDate))
            .build();
        orderRepository.save(order);
        return order;
    }
    
    public void purchaseAll(Long productId) {
        Cart cart = getCart();
        cart.remove(productId);
        saveCart(cart);
    }
}
