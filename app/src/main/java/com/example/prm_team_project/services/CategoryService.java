package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.models.requests.NewCategoryRequest;
import com.example.prm_team_project.models.requests.UpdateCategoryRequest;
import com.google.common.collect.Lists;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@RequiresApi(api = 33)
public class CategoryService extends BaseService {
    
    public Category findById(Long id) {
        return categoryRepository.findOneOrThrow(id, "Category not found");
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }
    
    public List<Category> findAll(Collection<Long> ids) {
        if (!isSynchronizedData()) {
            return Lists.newArrayList();
        }
        return categoryRepository.findAll(ids);
    }
    
    public Category addCategory(NewCategoryRequest request, InputStream cover) {
//        checkSyncData();
        validate(request);
        Category category = request.toCategory();
        System.out.println(cover);
        saveCoverFor(category, cover);
        return categoryRepository.save(category);
    }
    
    private void saveCoverFor(final Category category, final InputStream cover) {
        try {
            if (cover != null) {
                final CountDownLatch latch = new CountDownLatch(1);
                new Thread(() -> {
                    String coverPath = uploadService.uploadFile(cover).orElse(null);
                    category.setImage(coverPath);
                    latch.countDown();
                }).start();
                latch.await();
            }
        } catch (Exception ignored) {
        
        }

    }
    
    public Category updateCategory(UpdateCategoryRequest request, InputStream cover) {
//        checkSyncData();
    
        validate(request);
        Category category = categoryRepository.findOneOrThrow(request.getCategoryId(), "Category not found");
        Category updateCategory = request.updateFor(category);
        saveCoverFor(updateCategory, cover);
        return categoryRepository.save(category);
    }
    
    public boolean deleteCategory(Long id) {
//        checkSyncData();
//    
        Category category = categoryRepository.findOneOrThrow(id, "Category not found");
        return categoryRepository.delete(category);
    }
}
