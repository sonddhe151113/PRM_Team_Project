package com.example.prm_team_project.services;

import android.content.Context;
import androidx.annotation.RequiresApi;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.IOUtils;
import com.google.common.collect.Maps;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RequiresApi(api = 33)
//@Log4j2
public class UploadService {
    
    private static final Integer DEFAULT_EXP_TIME_IN_SECONDS = 60 * 60 * 10000;
    
    private static final String USER_METADATA_KEY_PREFIX = "x-amz-meta-";
    
    private static final Map<String, String> DEFAULT_USER_METADATA = Maps.newHashMap();
    
    private static final String url = "https://s3.ap-southeast-1.amazonaws.com";
    
    private static final String region = "ap-southeast-1";
    
    private static final String bucketName = "anantacode";
    
    private static final String accessKey = "AKIAYHIJHQDPX64JCAN5";
    
    private static final String secretKey = "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";
    
    private static AmazonS3 s3Client;
    
    static {
        DEFAULT_USER_METADATA.put("owner", "AnantaCode");
    }
    
    public void initStorage(Context context) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        s3Client = new AmazonS3Client(credentials);
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        s3Client.setEndpoint("https://s3-ap-southeast-1.amazonaws.com/");
    }
    
    public Optional<String> uploadFile(@Nullable InputStream file) {
        try {
            if (file == null) {
                return Optional.empty();
            }
            String fileName = generateFileName("file");
            String downloadUrl = getDownloadUrl(fileName);
            final byte[] content = IOUtils.toByteArray(file);
            ObjectMetadata metadata = getMetaData();

            uploadFileTos3bucket(fileName, content, metadata);
//            log.info("Upload successfully to S3. You can access file with following url: {}", downloadUrl);
            return Optional.of(downloadUrl);
        } catch (Exception e) {
//            log.warn("Upload failed to S3.");
            e.printStackTrace();
            return Optional.empty();
        }
    }
    
    private @NotNull ObjectMetadata getMetaData() {
        ObjectMetadata metadata = new ObjectMetadata();
        DEFAULT_USER_METADATA.forEach((key, value) -> metadata.addUserMetadata(USER_METADATA_KEY_PREFIX + key, value));
        return metadata;
    }

    private void uploadFileTos3bucket(String fileName, byte[] content, ObjectMetadata metadata) {
        s3Client.putObject(
            new PutObjectRequest(bucketName, fileName, new ByteArrayInputStream(content), metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead)
        );
    }

    private String getDownloadUrl(@NotNull String fileName) {
        return String.format("https://%s.s3.amazonaws.com/%s", bucketName, fileName);
    }

    private String generateFileName(@NotNull String originalFilename) {
        String uniqueId = Long.toHexString(UUID.randomUUID().getMostSignificantBits());

        String uploadTime = LocalDateTime.now().toString();
        String fileExtension = "jpg";

        return String.format("%s_%s.%s", uniqueId, uploadTime, fileExtension);
    }

    public void deleteFile(@NotNull String url) {
        AmazonS3URI uri = new AmazonS3URI(url);
        s3Client.deleteObject(uri.getBucket(), uri.getKey());
    }

    public Optional<String> generatePresignedUrl(@NotNull String url) {
        return generatePresignedUrl(url, DEFAULT_EXP_TIME_IN_SECONDS);
    }

    public Optional<String> generatePresignedUrl(@NotNull String url, int expireTimeInSecond) {
        try {
            String objectKey = extractObjectKeyFrom(url);
            URL presignedUrl = generatePresignUrlWith(objectKey, expireTimeInSecond);
            return Optional.of(presignedUrl.toString());
        } catch (AmazonServiceException e) {
//            log.error("The call was transmitted successfully, but Amazon S3 couldn't process for {} ", url);
            e.printStackTrace();
        } catch (Exception e) {
//            log.error(
//                "Amazon S3 couldn't be contacted for a response, or the client, thus presigning for {} has failed",
//                url
//            );
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private URL generatePresignUrlWith(@NotNull String objectKey, int expireTimeInSecond) {
        GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, objectKey)
            .withMethod(HttpMethod.GET)
            .withExpiration(Date.from(ZonedDateTime.now().plusSeconds(expireTimeInSecond).toInstant()));

        return s3Client.generatePresignedUrl(request);
    }

    private @NotNull String extractObjectKeyFrom(@NotNull String url) {
        String objectKey = url;
        if (url.contains("amazonaws.com/")) {
            String[] parts = url.split("amazonaws\\.com/");
            if (parts.length > 1) {
                objectKey = parts[1];
            }
        }
        if (objectKey.startsWith(bucketName + "/")) {
            objectKey = objectKey.replaceFirst("^" + bucketName + "/", "");
        }
        return objectKey;
    }
}