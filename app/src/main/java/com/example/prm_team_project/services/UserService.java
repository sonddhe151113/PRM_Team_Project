package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.models.requests.UpdateProfileRequest;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@AllArgsConstructor
@RequiresApi(api = 33)
public class UserService extends BaseService  {
    public List<User> findAllUsers() {
        return Lists.newArrayList();
    }
    
    public List<User> findAllUser(String usernameOrEmail, User.Gender gender, User.Role role) {
        return Lists.newArrayList();
    }
    
    public User updateProfile(UpdateProfileRequest request) {
        checkSyncData();
        validate(request);
        User user = userRepository.findOneOrThrow(request.getUserId(), "User not found");
        User updatedUser = request.updateFor(user);
        return userRepository.save(updatedUser);
    }
    
    public User updateAvatar(Long userId, InputStream avatar) {
        checkSyncData();
        User user = userRepository.findOneOrThrow(userId, "User not found");
        try {
            if (avatar != null) {
                final CountDownLatch latch = new CountDownLatch(1);
                new Thread(() -> {
                    String avatarPath = uploadService.uploadFile(avatar).orElse(null);
                    user.setAvatar(avatarPath);
                    latch.countDown();
                }).start();
                latch.await();
            }
        } catch (Exception ignored) {
        
        }
        return userRepository.save(user);
    }
}
