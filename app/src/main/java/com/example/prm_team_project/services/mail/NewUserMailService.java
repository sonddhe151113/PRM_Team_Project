package com.example.prm_team_project.services.mail;

import lombok.Setter;
import lombok.experimental.Accessors;
import org.jetbrains.annotations.NotNull;

@Accessors(chain = true)
@Setter
public class NewUserMailService extends BaseMailService {
    
    private String username;
    private static final String CONTENT_TEMPLATE =
        "Hello %s, welcome to our website, before that, we need you to verify the account that you has used to join our website. Please click the following link to verify: ";
    
    @Override
    protected String getSubject(@NotNull String email) {
        return "Welcome to our website";
    }
    
    @Override
    protected String getContent(@NotNull String email) {
        return String.format(CONTENT_TEMPLATE, username);
    }
    
    @Override
    public String getSuccessMessageAfterSentToMail(@NotNull String email) {
        return "Email has sent successfully to " + email;
    }
}
