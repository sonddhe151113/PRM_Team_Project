package com.example.prm_team_project.services.test;

import android.os.Environment;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.models.dtos.ApiException;
import com.example.prm_team_project.models.dtos.ValidationException;
import com.example.prm_team_project.models.requests.NewCategoryRequest;
import com.example.prm_team_project.models.requests.SignInRequest;
import com.example.prm_team_project.models.requests.UpdateCategoryRequest;
import com.example.prm_team_project.services.AuthService;
import com.example.prm_team_project.services.CategoryService;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

@RequiresApi(api = 33)
public class TestCategory {
    public static void testAuth() {
        AuthService authService = new AuthService();
//        authService.resetPassword(1L);

//        try {
//            NewUserRequest sondd = NewUserRequest.builder()
//                .email("sonddhe151113@fpt.edu.vn")
//                .username("Sondd")
//                .password("12345678aA")
//                .confirmPassword("12345678aA")
//                .build();
//            authService.signUp(sondd);
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            if (e instanceof ValidationException) {
//                ((ValidationException) e).getErrors().forEach((error, message) -> {
//                    System.out.println(String.format("Error on field %s with message: %s", error, message));
//                });
//                return;
//            }
//            e.printStackTrace();
//        }
        try {
            SignInRequest login = SignInRequest.builder()
                .email("sonddhe151113@fpt.edu.vn")
                .password("12345678aA")
                .build();
        
            authService.signIn(login);
        } catch (Exception e) {
            e.printStackTrace();
        
            if (e instanceof ValidationException) {
                ((ValidationException) e).getErrors().forEach((error, message) -> {
                    System.out.println(String.format("Error on field %s with message: %s", error, message));
                });
                return;
            }
            e.printStackTrace();
        }
    }
    
    public static void test() {
        testUpdateCategory();
    }
    
    public static void testAddCategory() {
        CategoryService service = new CategoryService();
        try {
            String path = Environment.getExternalStorageDirectory().getPath() + "/screenshot.png";
            File file = new File(path);
            DataInputStream inputStream = new DataInputStream(new FileInputStream(file));
            service.addCategory(NewCategoryRequest.builder().name("Food").description("cancer things").build(), inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            
            if (e instanceof ValidationException) {
                ((ValidationException) e).getErrors().forEach((error, message) -> {
                    System.out.println(String.format("Error on field %s with message: %s", error, message));
                });
                return;
            }
            e.printStackTrace();
        }
    }
    
    public static void testUpdateCategory() {
        try {
            CategoryService service = new CategoryService();
            service.updateCategory(UpdateCategoryRequest.builder().categoryId(1L).name("Something new").description("Still cancer").build(), null);
        } catch (Exception e) {
            e.printStackTrace();
            throw ApiException.withMessage(e.getMessage());
        }
    }
}
