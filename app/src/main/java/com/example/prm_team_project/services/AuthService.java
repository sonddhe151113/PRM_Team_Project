package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.BaseEntity;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.models.dtos.ApiException;
import com.example.prm_team_project.models.requests.ChangePasswordRequest;
import com.example.prm_team_project.models.requests.NewCustomerRequest;
import com.example.prm_team_project.models.requests.NewUserRequest;
import com.example.prm_team_project.models.requests.SignInRequest;
import com.example.prm_team_project.services.mail.NewUserMailService;
import com.example.prm_team_project.services.mail.ResetPasswordService;
import com.example.prm_team_project.statics.Apps;
import com.example.prm_team_project.utils.ApiStrings;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
@RequiresApi(api = 33)
public class AuthService extends BaseService  {
    
    public static final String CURRENT_USER_KEY = "userId";
    
    public void signIn(SignInRequest request) {
//        if (!Apps.isSynchronizedData()) {
//            return;
//        }
        validate(request);
        Long userId = userRepository.findOne(request.getEmail()).map(BaseEntity::getId).orElse(null);
        Apps.saveToLocal(CURRENT_USER_KEY, userId);
        System.out.println("Login successfully");
    }
    
    public void signOut() {
        if (!Apps.isSynchronizedData()) {
            return;
        }
        Apps.saveToLocal(CURRENT_USER_KEY, null);
    }
    
    public void signUp(NewUserRequest request) {
//        if (!Apps.isSynchronizedData()) {
//            return;
//        }
        validate(request);
        String password = request.getPassword();
        User user = User.builder()
            .email(request.getEmail())
            .username(request.getUsername())
            .role(User.Role.CUSTOMER)
            .gender(User.Gender.MALE)
            .status(User.Status.ACTIVE)
            .password(ApiStrings.getEncryptedPassword(password)).build();
        User savedUser = userRepository.save(user);
    
        CustomerService customerService = new CustomerService();
        customerService.addCustomer(NewCustomerRequest.builder().userId(savedUser.getId()).build());
    
        new NewUserMailService().setUsername(user.getUsername()).sendTo(user.getEmail());
        System.out.println("Sign up successfully with email: " + request.getEmail());
    }
    
    public void changePassword(final ChangePasswordRequest request) {
//        if (!Apps.isSynchronizedData()) {
//            return;
//        }
        validate(request);
        User user = userRepository.getOne(request.getUserId());
        user.setPassword(ApiStrings.getEncryptedPassword(request.getNewPassword()));
        userRepository.save(user);
    }
    
    public void resetPassword(Long userId) {
        if (!Apps.isSynchronizedData()) {
            return;
        }
        User user = userRepository.findOneOrThrow(userId, "User not found");
        String randomPassword = ApiStrings.generateRandomStringWithLength(12);
        user.setPassword(ApiStrings.getEncryptedPassword(randomPassword));
        userRepository.save(user);
        new ResetPasswordService().setPassword(randomPassword).sendTo(user.getEmail());
        System.out.println("===================// LOG // ===================");
        System.out.println("ResetPassword successfully");
        System.out.println("===================// LOG // ===================");
    }
    
    public static User getCurrentUser() {
        Long userId = Apps.getLocalLongValue(CURRENT_USER_KEY);
        return Optional.ofNullable(userId).map(id -> userRepository.getOne(id)).orElseThrow(() -> ApiException.withMessage("User not found"));
    }
}
