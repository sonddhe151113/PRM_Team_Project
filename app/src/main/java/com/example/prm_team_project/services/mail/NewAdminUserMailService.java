package com.example.prm_team_project.services.mail;

import lombok.Setter;
import lombok.experimental.Accessors;
import org.jetbrains.annotations.NotNull;

@Accessors(chain = true)
@Setter
public class NewAdminUserMailService extends BaseMailService {
    
    private String username;
    private String password;
    private static final String CONTENT_TEMPLATE =
        "Hello %s, welcome to our website. You can access our system using following account. Email: %s. Password: %s";
    
    @Override
    protected String getSubject(@NotNull String email) {
        return "Welcome to our website";
    }
    
    @Override
    protected String getContent(@NotNull String email) {
        return String.format(CONTENT_TEMPLATE, username, email, password);
    }
    
    @Override
    public String getSuccessMessageAfterSentToMail(@NotNull String email) {
        return "Email has sent successfully to " + email;
    }
}
