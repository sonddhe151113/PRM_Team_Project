package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.models.requests.NewProductRequest;
import com.example.prm_team_project.models.requests.UpdateProductRequest;
import com.google.common.collect.Lists;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@RequiresApi(api = 33)
public class ProductService extends BaseService  {
    public Product add(NewProductRequest request, InputStream cover) {
        checkSyncData();
        validate(request);
        Product product = request.toProduct();
        if (cover != null) {
            String coverPath = uploadService.uploadFile(cover).orElse(null);
            product.setCover(coverPath);
        }
        productRepository.save(product);
        return product;
    }
    public Product update(UpdateProductRequest request, InputStream cover) {
        checkSyncData();
        validate(request);
        Product product = productRepository.findOneOrThrow(request.getProductId(), "Product not found");
        try {
            if (cover != null) {
                final CountDownLatch latch = new CountDownLatch(1);
                new Thread(() -> {
                    String coverPath = uploadService.uploadFile(cover).orElse(null);
                    product.setCover(coverPath);
                    latch.countDown();
                }).start();
                latch.await();
            }
        } catch (Exception ignored) {
        
        }
        Product needUpdateProduct = request.updateFor(product);
        productRepository.save(needUpdateProduct);
     
        return product;
    }
    
    public void delete(Long productId) {
        checkSyncData();
        productRepository.delete(productId);
    }
    
    public Product findById(Long productId) {
        return productRepository.findOneOrThrow(productId, "Product not found");
    }
    
    public List<Product> findAll(List<Long> productIds) {
        if (!synchronizedData) {
            return Lists.newArrayList();
        }
        return productRepository.findAll(productIds);
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }
    
    
}
