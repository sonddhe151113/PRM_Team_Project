package com.example.prm_team_project.services.mail;


import lombok.experimental.Accessors;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Accessors(chain = true)
public abstract class BaseMailService {
    
    private static final String port = "465";
    
    private static final String host = "smtp.gmail.com";
    
    private static final String username = "anantacode0810@gmail.com";
    
    private static final String password = "qmvexehrxvgtzcok";
    
    private @NotNull Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.port", port);
        return props;
    }
    
    @Contract("_ -> new")
    private @NotNull Session getSession(Properties props) {
        return Session.getInstance(props, getAuthentication());
    }
    
    @Contract(value = " -> new", pure = true)
    private @NotNull Authenticator getAuthentication() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
    }
    
    /**
     * Send mail to the email
     * @param email: Email of the one who will be sent to
     * @return true if the email sent successfully, otherwise false.
     */
    public final boolean sendTo(@NotNull String email) {
        Properties properties = getProperties();
        Session session = getSession(properties);
        
        try {
            MimeMessage message = getMessage(email, session);
            new Thread(() -> {
                try {
                    Transport.send(message);
                    System.out.println(getSuccessMessageAfterSentToMail(email));
                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
            }).start();
            return true;
        } catch (MessagingException e) {
//            log.warn("There are some errors while sending mail.");
//            log.warn(e.getMessage());
            
            return false;
        }
    }
    
    private @NotNull MimeMessage getMessage(@NotNull String email, @NotNull Session session) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setSubject(getSubject(email));
        message.setText(getContent(email));
        
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
        return message;
    }
    
    /**
     *
     * @return [String] the subject of the mail that will be sent.
     */
    protected abstract String getSubject(@NotNull String email);
    
    /**
     *
     * @return the content of the mail that will be sent.
     */
    protected abstract String getContent(@NotNull String email);
    
    /**
     * @return the success message that will be shown in  the console.
     */
    public abstract String getSuccessMessageAfterSentToMail(@NotNull String email);
}
