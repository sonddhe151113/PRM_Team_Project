package com.example.prm_team_project.services;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.models.dtos.ApiException;
import com.example.prm_team_project.models.dtos.ValidationException;
import com.example.prm_team_project.models.requests.BaseRequest;
import com.example.prm_team_project.statics.Apps;
import com.example.prm_team_project.validators.BaseValidator;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@RequiresApi(api = 33)
public class BaseService extends Apps {
    
    protected void validate(@NotNull BaseRequest request) {
        BaseValidator validator = Optional
            .ofNullable(request.getClass().getAnnotation(ValidatedBy.class))
            .map(ValidatedBy::value)
            .map(clazz -> {
                try {
                    Constructor<? extends BaseValidator> constructor = clazz.getConstructor();
                    return constructor.newInstance();
                
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    throw ApiException.withMessage(String.format("Validator class %s should have @NoArgsConstructor", request.getClass().getSimpleName()));
                }
            })
            .orElseThrow(() -> ApiException.withMessage("% request has no validator. Please annotate it with @ValidatedBy"));
        validator.validate(request);
        if (validator.hasErrors()) {
            throw ValidationException.withErrors(validator.getErrors());
        }
    }
}
