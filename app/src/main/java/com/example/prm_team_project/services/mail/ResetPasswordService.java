package com.example.prm_team_project.services.mail;

import lombok.Setter;
import lombok.experimental.Accessors;
import org.jetbrains.annotations.NotNull;

@Accessors(chain = true)
@Setter
public class ResetPasswordService extends BaseMailService {
    
    private String password;
    
    private static final String SUBJECT = "Request to reset password";
    private static final String CONTENT =
        "Hello, %s.\n" +
        "        ** This is an automated message -- please do not reply as you will not receive a response. **\n" +
        "        This message is in response to your request to reset your account password.\n" +
        "\n" +
        "        Your password is: %s.";
    
    @Override
    protected String getSubject(@NotNull String email) {
        return SUBJECT;
    }
    
    @Override
    protected String getContent(@NotNull String email) {
        return String.format(CONTENT, email, password);
    }
    
    @Override
    public String getSuccessMessageAfterSentToMail(@NotNull String email) {
        return String.format("Sent reset message to %s", email);
    }
}
