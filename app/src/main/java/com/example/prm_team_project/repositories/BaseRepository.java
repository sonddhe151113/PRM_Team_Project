package com.example.prm_team_project.repositories;

import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.RawQuery;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;
import com.example.prm_team_project.entities.BaseEntity;
import com.example.prm_team_project.models.dtos.ApiException;
import com.example.prm_team_project.models.dtos.SqlFailedException;
import com.example.prm_team_project.statics.Apps;
import com.example.prm_team_project.utils.ApiCollections;
import com.example.prm_team_project.utils.ApiReflections;
import com.example.prm_team_project.utils.ApiStrings;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public interface BaseRepository<T extends BaseEntity> {
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default void syncDataToFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference(getTableName());
        List<T> all = findAll();
            reference.setValue(all);
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default void syncDataFromFirebase(final Runnable onSuccessAction) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference(getTableName());
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = 33)
            @Override
            public void onDataChange(@NonNull @NotNull final DataSnapshot snapshot) {
                ArrayList<T> values = Lists.newArrayList();
                Class<T> genericClass = (Class<T>) getGenericClass();
                for (DataSnapshot child : snapshot.getChildren()) {
                    try {
                        T value =  child.getValue(genericClass);
                        values.add(value);
                    } catch (Exception ignored) {
                        //
                    }
                }
                saveWithoutSync(values);
                Apps.synchronizedData = true;
                System.out.println("=================== // SYNCHRONIZED DATA TO FIREBASE //===================");
                if (onSuccessAction != null) {
                    onSuccessAction.run();
                }
            }
    
            @Override
            public void onCancelled(@NonNull @NotNull final DatabaseError error) {
        
            }
        });
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default void syncDataFromFirebase() {
        syncDataFromFirebase(null);
    }
    
    @SneakyThrows
    @RequiresApi(api = Build.VERSION_CODES.N)
    default String getTableName() {
        Class<?> clazz = getGenericClass();
        String tablePostFix = "s";
        return ApiStrings.snakeCaseOf(clazz.getSimpleName()) + tablePostFix;
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    private Class<?> getGenericClass() {
        return ApiReflections.genericTypeFromInterfaceOf(getClass().getInterfaces()[0]).orElseThrow(() -> new IllegalArgumentException("No generic type found"));
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default T getOne(Long id) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where id = %s limit 1", getTableName(), id)
        );
        return doGetOne_(query);
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default T getOneByField(String field, Object value) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where %s = '%s' limit 1", getTableName(), field, String.valueOf(value))
        );
        return doGetOne_(query);
    }
    
    @RawQuery
    T doGetOne_(SupportSQLiteQuery query);
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default Optional<T> findOne(Long id) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where id = %s limit 1", getTableName(), id)
        );
        return doFindOne_(query);
    }
    
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default T findOneOrThrow(Long id, String notFoundMessage) {
        return findOne(id).orElseThrow(() -> ApiException.withMessage(notFoundMessage));
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default Optional<T> findOneByField(String field, Object value) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where %s = '%s' limit 1", getTableName(), field, String.valueOf(value))
        );
        return doFindOne_(query);
    }
    
    @RawQuery
    Optional<T> doFindOne_(SupportSQLiteQuery query);
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default List<T> findAll() {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            "select * from " + getTableName()
        );
        return doFindAll_(query);
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default List<T> findAll(Collection<Long> ids) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where id in (%s)", getTableName(), Joiner.on(", ").join(ids))
        );
        return doFindAll_(query);
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default List<T> findAllByField(String field, Object value) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where %s = %s", getTableName(), field, String.valueOf(value))
        );
        return doFindAll_(query);
    }
    
    @RawQuery
    List<T> doFindAll_(SupportSQLiteQuery query);
    
    @RequiresApi(api = 33)
    default T save(T entity) {
        try {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            entity.setCreatedAt(now);
            entity.setUpdatedAt(now);
            Long id = saveOne(entity);
            entity.setId(id);
            syncDataToFirebase();
            return entity;
        } catch (Exception e) {
            throw SqlFailedException.withMessage("Save model failed");
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    default List<T> save(Collection<T> entities) {
        try {
            List<T> list = ApiCollections.listOf(entities).stream().filter(Objects::nonNull).collect(Collectors.toList());
            Timestamp now = new Timestamp(System.currentTimeMillis());
    
            list.forEach(item -> {
                if (item.getCreatedAt() == null) {
                    item.setCreatedAt(now);
                }
                item.setUpdatedAt(now);
            });
            List<Long> ids = saveAll(entities);
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setId(ids.get(i));
            }
            syncDataToFirebase();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw SqlFailedException.withMessage("Save model failed");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    default List<T> saveWithoutSync(Collection<T> entities) {
        try {
            List<T> list = ApiCollections.listOf(entities).stream().filter(Objects::nonNull).collect(Collectors.toList());
            Timestamp now = new Timestamp(System.currentTimeMillis());
    
            list.forEach(item -> {
                if (item.getCreatedAt() == null) {
                    item.setCreatedAt(now);
                }
                item.setUpdatedAt(now);
            });
            List<Long> ids = saveAll(entities);
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setId(ids.get(i));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw SqlFailedException.withMessage("Save model failed");
        }
    }
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long saveOne(T entity);
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> saveAll(Collection<T> entities);
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default boolean delete(Long id) {
        try {
            SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                String.format("delete from %s where id = %s", getTableName(), id)
            );
            doDelete_(query);
            syncDataToFirebase();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default boolean delete(T entity) {
        return delete(entity.getId());
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default boolean deleteAllByIds(List<Long> ids) {
        try {
            if (ids == null || ids.isEmpty()) {
                return true;
            }
            String items = String.format("(%s)", Joiner.on(", ").join(ids));
            SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                String.format("delete from %s where id in (%s)", getTableName(), items)
            );
            doDelete_(query);
            syncDataToFirebase();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default boolean deleteAll(List<T> entities) {
        return deleteAllByIds(entities.stream().map(BaseEntity::getId).collect(Collectors.toList()));
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default boolean deleteAll(T... entities) {
        return deleteAllByIds(Lists.newArrayList(entities).stream().map(BaseEntity::getId).collect(Collectors.toList()));
    }
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    default boolean deleteAll(Long... ids) {
        return deleteAllByIds(Lists.newArrayList(ids));
    }
    
    @RawQuery
    boolean doDelete_(SupportSQLiteQuery query);
}

