package com.example.prm_team_project.repositories;

import androidx.room.Dao;
import com.example.prm_team_project.entities.Category;

@Dao
public interface CategoryRepository extends BaseRepository<Category> {
    default String getTableName() {
        return "categories";
    }
}
