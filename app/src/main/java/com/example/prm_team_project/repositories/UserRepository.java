package com.example.prm_team_project.repositories;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.room.Dao;
import androidx.sqlite.db.SimpleSQLiteQuery;
import com.example.prm_team_project.entities.User;

import java.util.Optional;

@Dao
public interface UserRepository extends BaseRepository<User> {
    @RequiresApi(api = Build.VERSION_CODES.N)
    default Optional<User> findOne(String email) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            String.format("select * from %s where email = '%s' limit 1", getTableName(), email)
        );

        return doFindOne_(query);
    }
    
}
