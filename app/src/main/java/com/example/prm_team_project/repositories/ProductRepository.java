package com.example.prm_team_project.repositories;

import androidx.room.Dao;
import com.example.prm_team_project.entities.Product;

@Dao
public interface ProductRepository extends BaseRepository<Product> {
}
