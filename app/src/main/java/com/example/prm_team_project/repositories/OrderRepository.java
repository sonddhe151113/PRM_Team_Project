package com.example.prm_team_project.repositories;

import androidx.room.Dao;
import com.example.prm_team_project.entities.Order;

@Dao
public interface OrderRepository extends BaseRepository<Order> {
}
