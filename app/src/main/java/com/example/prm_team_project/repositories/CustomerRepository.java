package com.example.prm_team_project.repositories;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.room.Dao;
import com.example.prm_team_project.entities.Customer;

@RequiresApi(api = Build.VERSION_CODES.N)
@Dao
public interface CustomerRepository extends BaseRepository<Customer> {
    default Customer getOneByUserId(Long id) {
        return getOneByField("userId", id);
    }
}
