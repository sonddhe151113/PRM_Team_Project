package com.example.prm_team_project.repositories;

import androidx.room.Dao;
import com.example.prm_team_project.entities.OrderDetail;

@Dao
public interface OrderDetailRepository extends BaseRepository<OrderDetail> {
}
