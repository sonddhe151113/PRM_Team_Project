package com.example.prm_team_project.utils;


import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.experimental.UtilityClass;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiresApi(api = Build.VERSION_CODES.N)
@UtilityClass
public class ApiReflections {
    
    /**
     * @author Sondd
     * Check if a class is extended from other class.
     * @param parentClass can be null.
     * @param clazz can be null.
     * @return true if the class extends or implements the parent class or the input classes is null, otherwise false.
     */
    public static boolean isChildClassOf(@Nullable Class<?> parentClass, @Nullable Class<?> clazz) {
        if (Objects.isNull(parentClass) || Objects.isNull(clazz)) {
            return false;
        }
        List<Class<?>> classes = getAncestorClasses(clazz);
        return classes.stream().anyMatch(c -> c.isAssignableFrom(parentClass));
    }
    
    /**
     * @author Sondd
     * Get all fields name of a class. Those fields including private and public fields in
     * that one class and its ancestors.
     * @param clazz The class that you want to extract. Class can be null.
     * @return empty list if class is null. Otherwise, return list of field's name.
     */
    public static List<String> getFieldNamesOf(@Nullable Class<?> clazz) {
        if (Objects.isNull(clazz)) {
            return Lists.newArrayList();
        }
        return getFieldsOf(clazz).stream().map(Field::getName).collect(Collectors.toList());
    }
    
    /**
     * @author Sondd
     * Get all fields name of a class. Those fields including private and public fields in
     * that one class and its ancestors.
     * @param clazz The class that you want to extract. Class can be null.
     * @return empty set if class is null. Otherwise, return set of field's name.
     */
    public static Set<String> getFieldNameSetOf(@Nullable Class<?> clazz) {
        return Sets.newHashSet(getFieldNamesOf(clazz));
    }
    
    /**
     * @author Sondd
     * Get all fields of a class. Those fields including private and public fields in
     * that one class and its ancestors.
     * @param clazz The class that you want to extract. Class can be null.
     * @return empty list if class is null, otherwise return its fields.
     */
    public static List<Field> getFieldsOf(@Nullable Class<?> clazz) {
        if (Objects.isNull(clazz)) {
            return Lists.newArrayList();
        }
        List<Class<?>> classes = getAncestorClasses(clazz);
        return classes.stream().map(Class::getDeclaredFields).flatMap(Stream::of).collect(Collectors.toList());
    }
    
    /**
     * @author Sondd
     * Get all non-static fields of a class. Those fields including private and public fields in
     * that one class and its ancestors.
     * @param clazz The class that you want to extract. Class can be null.
     * @return empty list if class is null, otherwise return its fields.
     */
    public static List<Field> getNonStaticFieldsOf(@Nullable Class<?> clazz) {
        if (Objects.isNull(clazz)) {
            return Lists.newArrayList();
        }
        List<Class<?>> classes = getAncestorClasses(clazz);
        return classes.stream().map(Class::getDeclaredFields).flatMap(Stream::of).filter(field -> !Modifier.isStatic(field.getModifiers())).collect(Collectors.toList());
    }
    
    /**
     * @author Sondd
     * Find field of a class based on its name (ignore case).
     * @param fieldName name of the field. If null then return Optional.empty()
     * @param clazz class which contains the field. If null then return Optional.empty().
     * @return Empty if input is null or can't find the field.
     */
    public static Optional<Field> getField(@Nullable String fieldName, @Nullable Class<?> clazz) {
        if (Objects.isNull(fieldName)|| Objects.isNull(clazz)) {
            return Optional.empty();
        }
        final List<Field> fields = getFieldsOf(clazz);
        return fields.stream().filter(field -> field.getName().equalsIgnoreCase(fieldName)).findFirst();
    }
    
    /**
     * @author Sondd
     * Get field's value of a object.
     * @param field field of the object. Field can be null.
     * @param object object that contains the value. Object can be null.
     * @return Optional.empty() if input is null or field can not be accessed. Otherwise, return Optional of object.
     */
    public static Optional<?> getFieldValue(@Nullable Field field, @Nullable Object object) {
        if (Objects.isNull(field) || Objects.isNull(object)){
            return Optional.empty();
        }
        
        Object value;
        try {
            field.setAccessible(true);
            value = field.get(object);
        } catch (IllegalAccessException e) {
            value = null;
        }
        field.setAccessible(false);
        return Optional.ofNullable(value);
    }
    
    /**
     * @author Sondd
     * Get field's value of a object.
     * @param fieldName field of the object. Field can be null.
     * @param object object that contains the value. Object can be null.
     * @return Optional.empty() if input is null or field can not be accessed. Otherwise, return Optional of object.
     */
    @RequiresApi(api = 33)
    public static Optional<?> getFieldValue(@Nullable String fieldName, @Nullable Object object) {
        if (Objects.isNull(fieldName) || Objects.isNull(object)){
            return Optional.empty();
        }
        Optional<Field> fieldOpt = getField(fieldName, object.getClass());
        if (fieldOpt.isEmpty()){
            return Optional.empty();
        }
        Field field = fieldOpt.get();
        
        field.setAccessible(true);
        Object value;
        try {
            value = field.get(object);
        } catch (IllegalAccessException e) {
            value = null;
        } finally {
            field.setAccessible(false);
        }
        return Optional.ofNullable(value);
    }
    
    /**
     * @author Sondd
     * Get all annotations of a field as a list.
     * @param fieldName name of the field to extract. Field can be null.
     * @param clazz Class that contains the field. Class can be null.
     * @return Empty list if input are null or field not found. Otherwise, return list of annotations found.
     */
    @RequiresApi(api = Build.VERSION_CODES.R)
    public static List<Annotation> getFieldAnnotations(@Nullable String fieldName, @Nullable Class<?> clazz) {
        if (Objects.isNull(fieldName)|| Objects.isNull(clazz)) {
            return Lists.newArrayList();
        }
        return getField(fieldName, clazz).map(field -> List.of(field.getAnnotations())).orElseGet(Lists::newArrayList);
    }
    
    /**
     * @author Sondd
     * Get all annotations of a class and its ancestors.
     * @param clazz The class that you want to extract. Class can be null.
     * @return empty list if class is null. Otherwise, return annotations of class.
     */
    public static List<Annotation> getAnnotationsOf(@Nullable Class<?> clazz) {
        if (Objects.isNull(clazz)){
            return Lists.newArrayList();
        }
        List<Class<?>> classes = getAncestorClasses(clazz);
        return classes.stream().map(Class::getAnnotations).flatMap(Stream::of).collect(Collectors.toList());
    }
    
    /**
     * Get certain annotation of a class.
     * @param annotationClass Annotation you want to find. Can't be null.
     * @param clazz Class that you want to get annotation from. Can't be null.
     * @return Empty Optional if not found. Otherwise, return Optional of annotation.
     */
    public static <T extends Annotation> Optional<T> getAnnotation(final Class<T> annotationClass, final Class<?> clazz) {
        return getAnnotationsOf(clazz).stream()
            .filter(ann -> ann.annotationType().isAssignableFrom(annotationClass))
            .map(annotationClass::cast)
            .findFirst();
    }
    
    /**
     * Get certain annotation of a field.
     * @param annotationClass Annotation you want to find. Can't be null.
     * @param field Field that you want to get annotation from. Can't be null.
     * @return Empty Optional if not found. Otherwise, return Optional of annotation.
     */
    public static <T> Optional<T> getAnnotation(final Class<T> annotationClass, final Field field) {
        return Arrays.stream(field.getAnnotations())
            .filter(ann -> ann.annotationType().isAssignableFrom(annotationClass))
            .map(annotationClass::cast)
            .findFirst();
    }
    
    /**
     * @author Sondd
     * Get all ancestors of a class including the class itself.
     * @param clazz The class that you want to extract. Class can be null.
     * @throws IllegalArgumentException If input class is null.
     */
    public static List<Class<?>> getAncestorClasses(Class<?> clazz) {
        Preconditions.checkNotNull(clazz, "Can't find ancestors of a null.");
        ArrayList<Class<?>> classes = Lists.newArrayList();
        Class<?> tempClass = clazz;
        while (tempClass != null) {
            classes.add(tempClass);
            tempClass = tempClass.getSuperclass();
        }
        return classes;
    }
    
    /**
     * @author Sondd
     * Check if a class has annotation or not.
     * @param annotationClass can be null.
     * @param clazz can be null.
     * @return false if input is null or class doesn't have annotation. Otherwise, return true.
     */
    public static boolean hasAnnotation(@Nullable Class<? extends Annotation> annotationClass, @Nullable Class<?> clazz) {
        if (Objects.isNull(annotationClass) || Objects.isNull(clazz)) {
            return false;
        }
        List<Annotation> annotations = getAnnotationsOf(clazz);
        return annotations.stream().anyMatch(annotation -> annotation.annotationType().equals(annotationClass));
    }
    
    /**
     * @author Sondd
     * Check if annotation is defined in certain package.
     * @param annotation can be null.
     * @param packageName can be null.
     * @return false if input is null or if annotation is not in the package. Otherwise, return true.
     */
    @RequiresApi(api = 31)
    public static boolean isOfPackage(@Nullable Annotation annotation, @Nullable String packageName) {
        if (Objects.isNull(annotation) || Objects.isNull(packageName)){
            return false;
        }
        return annotation.annotationType().getPackageName().equals(packageName);
    }
    
    /**
     * @author Sondd
     * Set field value for object.
     * @param object can be null
     * @param fieldName can be null
     * @param fieldValue can be null
     * @return true if set value successfully. Otherwise, return false.
     * If return false, it might be due to some follow reasons:<br/>
     * - Object or field name is null<br/>
     * - Field not found.
     */
    @RequiresApi(api = 33)
    public static boolean setFieldValue(@Nullable Object object, @Nullable String fieldName, @Nullable Object fieldValue) {
        if (Objects.isNull(object) || Objects.isNull(fieldName)) {
            return false;
        }
        
        final Optional<Field> fieldOpt = getField(fieldName, object.getClass());
        final boolean fieldNotFound = fieldOpt.isEmpty();
        
        if (fieldNotFound){
            return false;
        }
        final Field field = fieldOpt.get();
        try {
            field.setAccessible(true);
            field.set(object, fieldValue);
            field.setAccessible(false);
        } catch (IllegalAccessException e) {
            return false;
        }
        return true;
    }
    
    /**
     * Find all public methods of certain class.
     * @param clazz Class that you want to find methods.
     * @return Empty list if input is null. Otherwise, return list of public methods
     */
    public static List<Method> getPublicMethodsOf(@Nullable final Class<?> clazz) {
        if (clazz == null) {
            return Lists.newArrayList();
        }
        return Lists.newArrayList(clazz.getDeclaredMethods()).stream()
            .filter(method -> Modifier.isPublic(method.getModifiers()))
            .collect(Collectors.toList());
    }
    
    /**
     * Get generic type for superclass. NOTE: Support class with only one generic type.
     * @param clazz can be null. NOTE: This class must contains only one generic type.
     * @return Optional of generic type. Return empty if any exception caught.
     */
    public static Optional<Class<?>> genericTypeOf(@Nullable Class<?> clazz) {
        try {
            ParameterizedType genericClasses = (ParameterizedType) Objects.requireNonNull(clazz).getGenericSuperclass();
            Type type = genericClasses.getActualTypeArguments()[0];
            return Optional.of((Class<?>) type);
        } catch (ClassCastException | NullPointerException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
    /**
     * Get generic type for superclass. NOTE: Support class with only one generic type.
     * @param clazz can be null. NOTE: This class must contains only one generic type.
     * @return Optional of generic type. Return empty if any exception caught.
     */
    public static Optional<Class<?>> genericTypeFromInterfaceOf(@Nullable Class<?> clazz) {
        try {
            Type[] genericInterfaces = Objects.requireNonNull(clazz).getGenericInterfaces();
            ParameterizedType genericClasses = (ParameterizedType) genericInterfaces[0];
            Type type = genericClasses.getActualTypeArguments()[0];
            return Optional.of((Class<?>) type);
        } catch (ClassCastException | NullPointerException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
    
    /**
     * Check if a field is exists in certain class or not.
     * This method will ignore case while checking.
     * @param fieldName can be null.
     * @param clazz can be null.
     * @return false if inputs are null or when field is not exist in the class. Otherwise, return true.
     */
    public static boolean existField(@Nullable String fieldName, @Nullable Class<?> clazz) {
        if (fieldName == null || clazz == null) {
            return false;
        }
        return getFieldNamesOf(clazz).stream().anyMatch(field -> field.equalsIgnoreCase(fieldName));
    }
    
    
    /**
     * Get all annotations of a field
     * @param field can be null.
     * @return empty list if field is null. Otherwise, return annotations of the field.
     */
    public static
    List<Annotation> getAnnotationsOf(final @Nullable Field field) {
        if (field == null) {
            return Lists.newArrayList();
        }
        return Lists.newArrayList(field.getType().getDeclaredAnnotations());
    }
    
    /**
     * Check if certain field contains the annotation or not.
     * @param annotationClass can be null.
     * @param field can be null.
     * @return false if any input is null or field not contains annotation. Otherwise, return true.
     */
    public static boolean hasAnnotation(@Nullable final Class<? extends Annotation> annotationClass, @Nullable final Field field) {
        if (annotationClass == null || field == null) {
            return false;
        }
        return field.getAnnotation(annotationClass) != null;
    }
    
    /**
     * Check whether a field is collection or not.
     * @param field can be null.
     * @return false if input is null or field is not collection. Otherwise, return true.
     */
    public static boolean isCollection(@Nullable final Field field) {
        if (field == null) {
            return false;
        }
        return Collection.class.isAssignableFrom(field.getType());
    }
}
