package com.example.prm_team_project.utils;


import android.util.Base64;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Objects;

@UtilityClass
public class ApiStrings {
    public static String snakeCaseOf(String input) {
        if (input == null) {
            return "";
        }
        String regex = "([a-z])([A-Z]+)";
        String replacement = "$1_$2";
        return input.replaceAll(regex, replacement).toLowerCase();
    }
    
    private static final String SPACE = " ";
    private static final String UNDERSCORE = "_";
    private static final String EMPTY = "";
    private static final String COMMA = ", ";
    private static final String DOT = ", ";
    
    private static final int ASCII_START_VALUE = 33;
    private static final int ASCII_END_VALUE = 122;
    private static final boolean ALLOW_LETTERS = true;
    private static final boolean ALLOW_NUMBER = true;
    
    @NotNull
    public static String generateRandomStringWithLength(int length) {
        return RandomStringUtils.random(
            length,
            ASCII_START_VALUE,
            ASCII_END_VALUE,
            ALLOW_LETTERS,
            ALLOW_NUMBER,
            null,
            new SecureRandom()
        );
    }
    
    
    private String SECRET_KEY = "aesEncryptionKey";
    private String INIT_VECTOR = "encryptionIntVec";
    
    
    @SneakyThrows
    public static String getEncryptedPassword(String string) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(SECRET_KEY.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(string.getBytes());
            return Base64.encodeToString(encrypted, Base64.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    @SneakyThrows
    public static String getDecryptedPassword(String string) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(SECRET_KEY.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decode(string, Base64.DEFAULT));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
    
    public static boolean checkPassword(String inputPassword, String encryptedPassword) {
        String currentPassword = Objects.requireNonNull(getDecryptedPassword(encryptedPassword));
        return currentPassword.equalsIgnoreCase(inputPassword);
    }
}
