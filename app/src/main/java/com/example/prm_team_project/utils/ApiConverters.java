package com.example.prm_team_project.utils;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.input.UpdateAble;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class ApiConverters {
    private static final Gson gson = new Gson();
    
    public static String toJson(Object object) {
        return gson.toJson(object);
    }
    
    public static <T> T toObject(String json, Class<T> clazz) {
        if (StringUtils.isBlank(json)) {
            return null;
        }
        return gson.fromJson(json, clazz);
    }
    
    public static <T> T convertTo(Class<T> clazz, Object src) {
        return toObject(toJson(src), clazz);
    }
    
    @RequiresApi(api = 33)
    public static <T, S> T updateFor(T destiny, S source) {
        List<Field> updateAbleFields = ApiReflections.getNonStaticFieldsOf(source.getClass())
            .stream()
            .filter(field -> ApiReflections.hasAnnotation(UpdateAble.class, field))
            .collect(Collectors.toList());
        
        updateAbleFields.forEach(field -> ApiReflections.setFieldValue(destiny, field.getName(), ApiReflections.getFieldValue(field, source).orElse(null)));
    
        return destiny;
    }
}
