package com.example.prm_team_project.utils;

import lombok.experimental.UtilityClass;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@UtilityClass
public class ApiMoments {
    public static Timestamp timestampOf(LocalDateTime time) {
        return Timestamp.valueOf(time.toString());
    }
}
