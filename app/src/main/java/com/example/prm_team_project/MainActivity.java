package com.example.prm_team_project;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.Admin.CategoryAdaptorAdmin;
import com.example.prm_team_project.Adaptor.Admin.OrderAdaptorAdmin;
import com.example.prm_team_project.statics.Apps;

@RequiresApi(api = 33)
public class MainActivity extends AppCompatActivity {

    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter adapter2;
    private RecyclerView recyclerViewCategoryList;
    private RecyclerView recyclerViewPopularList;
    private RecyclerView recyclerViewOrderList;

    private ConstraintLayout btnStart;

    private CategoryAdaptorAdmin categoryAdaptorAdmin;
    private OrderAdaptorAdmin orderAdaptorAdmin;

    private EditText editTextCatName, editTextCatPic;
    private TextView txtOrderCusId, txtOrderAddress;
    private Button btnAdd, btnEdit, btnLoad, btnView;
    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_order);

        Apps.initRepo(this);
        Apps.initLocalStorage(this);
        Apps.autoSyncData(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
            if (Apps.isSynchronizedData()) {
                return;
            }
            setContentView(R.layout.activity_productbycategory);
//            CategoryService service = new CategoryService();
//            openFileChooser((uri) -> {
//                try {
//                    InputStream image = getContentResolver().openInputStream(uri);
//                    service.addCategory(NewCategoryRequest.builder().description("Something liquid").name("Some new Drink").build(), image);
//                } catch (Exception e) {
//                    throw new RuntimeException(e);
//                }
//            });
        });
//        testOfBackEndDoNotComment();
//        setContentView(R.layout.activity_splash);
//        btnStart = findViewById(R.id.btn_start);
//        btnStart.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, SigninActivity.class)));
//
//        RecyclerViewProductByCategory();
    }
    public void openFileChooser(ValueCallback<Uri> onSuccess) {
        this.onSuccess = onSuccess;
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        this.startActivityForResult(Intent.createChooser(i, "Image Chooser"), LAUNCH_SECOND_ACTIVITY);
    }

    ValueCallback<Uri> onSuccess;

    int LAUNCH_SECOND_ACTIVITY = 100;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            Uri result = (data == null || resultCode != RESULT_OK) ? null : data.getData();
            onSuccess.onReceiveValue(result);
        }
    }
//    public void RecyclerViewProductByCategory(){
//        List<Product> list = new ArrayList<>();
//        Product p1 = Product.builder()
//                .name("Product 1")
//                .unitPrice(123.0)
//                .cover("https://fpt.vn/storage/upload/images/site/fpt.png")
//                .build();
//        Product p2 = Product.builder()
//                .name("Product 2")
//                .unitPrice(123.0)
//                .cover("https://fpt.vn/storage/upload/images/site/fpt.png")
//                .build();
//        Product p3 = Product.builder()
//                .name("Product 3")
//                .unitPrice(123.0)
//                .cover("https://fpt.vn/storage/upload/images/site/fpt.png")
//                .build();
//
//        list.add(p1);
//        list.add(p2);
//        list.add(p3);
//
//        RecyclerView rec = findViewById(R.id.rec_productbycat);
//        ProductByCategoryAdaptor adapter = new ProductByCategoryAdaptor(list);
//        rec.setLayoutManager(new LinearLayoutManager(this));
//        rec.setAdapter(adapter);
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.N)
//    private void testOfBackEndDoNotComment() {
////        TestCategory.testAddCategory();
//
//    }
//
//    private void recyclerViewCategory(){
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        recyclerViewCategoryList = findViewById(R.id.recyclerView);
//        recyclerViewCategoryList.setLayoutManager(linearLayoutManager);
//
//        ArrayList<CategoryDomain> categories = new ArrayList<CategoryDomain>();
//        categories.add(new CategoryDomain("Pizza", "cat_1"));
//        categories.add(new CategoryDomain("Burger", "cat_2"));
//        categories.add(new CategoryDomain("Hotdog", "cat_3"));
//        categories.add(new CategoryDomain("Drink", "cat_4"));
//        categories.add(new CategoryDomain("Donut", "cat_5"));
//        adapter = new CategoryAdaptor(categories);
//        recyclerViewCategoryList.setAdapter(adapter);
//    }
//
//    private void recyclerViewPopular(){
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        recyclerViewPopularList = findViewById(R.id.recyclerView2);
//        recyclerViewPopularList.setLayoutManager(linearLayoutManager);
//
//        ArrayList<FoodDomain> foods = new ArrayList<FoodDomain>();
//        foods.add(new FoodDomain("pizza", "pizza", "pepper", 9.33));
//        foods.add(new FoodDomain("chesee burger", "pop_2", "chesse", 8.77));
//        adapter2 = new PopularAdaptor(foods);
//        recyclerViewPopularList.setAdapter(adapter2);
//    }
}