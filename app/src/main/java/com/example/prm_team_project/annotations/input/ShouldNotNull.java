package com.example.prm_team_project.annotations.input;


import com.example.prm_team_project.annotations.ValidatedFieldBy;
import com.example.prm_team_project.validators.FieldValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.FIELD })
@ValidatedFieldBy(value = ShouldNotNull.Validator.class)
public @interface ShouldNotNull {

    String message();

    class Validator extends FieldValidator<ShouldNotNull, Object> {
        public Validator(ShouldNotNull ann) {
            super(ann);
        }

        public boolean isValid(Object value) {
            return value != null;
        }
    }
}
