package com.example.prm_team_project.annotations.input;

import com.example.prm_team_project.annotations.ValidatedFieldBy;
import com.example.prm_team_project.validators.FieldValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.FIELD })
@ValidatedFieldBy(value = ShouldMeetPattern.Validator.class)
public @interface ShouldMeetPattern {

    String value();

    String message();

    class Validator extends FieldValidator<ShouldMeetPattern, String> {

        public Validator(ShouldMeetPattern ann) {
            super(ann);
        }

        public boolean isValid(String value) {
            String pattern = ann.value();
            return value.matches(pattern);
        }
    }
}
