package com.example.prm_team_project.annotations.input;

import com.example.prm_team_project.annotations.ValidatedFieldBy;
import com.example.prm_team_project.validators.FieldValidator;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.FIELD })
@ValidatedFieldBy(ShouldNotBlank.Validator.class)
public @interface ShouldNotBlank {

    String message();

    class Validator extends FieldValidator<ShouldNotBlank, String> {
        public Validator(ShouldNotBlank ann) {
            super(ann);
        }

        public boolean isValid(String value) {
            return StringUtils.isNotBlank(value);
        }
    }
}
