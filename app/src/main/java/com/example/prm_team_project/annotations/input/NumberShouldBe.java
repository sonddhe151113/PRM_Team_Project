package com.example.prm_team_project.annotations.input;

import com.example.prm_team_project.annotations.ValidatedFieldBy;
import com.example.prm_team_project.validators.FieldValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.FIELD })
@ValidatedFieldBy(value = NumberShouldBe.Validator.class)
public @interface NumberShouldBe {
    double largerThan() default Double.MIN_VALUE;
    double smallerThan() default  Double.MAX_VALUE;
    String message();
    
    class Validator extends FieldValidator<NumberShouldBe, Number> {
        
        public Validator(NumberShouldBe ann) {
            super(ann);
        }
        
        public boolean isValid(Number value) {
            if (value == null) {
                return false;
            }
            double min = ann.largerThan();
            double max = ann.smallerThan();
            double v = (double) value;
            return min < v && v < max;
        }
    }
}