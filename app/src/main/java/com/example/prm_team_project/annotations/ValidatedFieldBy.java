package com.example.prm_team_project.annotations;

import com.example.prm_team_project.validators.FieldValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface ValidatedFieldBy {
    Class<? extends FieldValidator> value();
}
