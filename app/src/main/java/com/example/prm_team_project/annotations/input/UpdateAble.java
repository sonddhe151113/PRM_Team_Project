package com.example.prm_team_project.annotations.input;

import com.example.prm_team_project.annotations.ValidatedFieldBy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.FIELD })
public @interface UpdateAble {
}
