package com.example.prm_team_project.annotations.input;

import com.example.prm_team_project.annotations.ValidatedFieldBy;
import com.example.prm_team_project.validators.FieldValidator;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.FIELD })
@ValidatedFieldBy(value = ShouldHaveLength.Validator.class)
public @interface ShouldHaveLength {
    int atLeast() default Integer.MIN_VALUE;
    int atMax() default Integer.MAX_VALUE;

    String message();

    class Validator extends FieldValidator<ShouldHaveLength, String> {

        public Validator(ShouldHaveLength ann) {
            super(ann);
        }

        public boolean isValid(String value) {
            int length = StringUtils.length(value);
            int min = ann.atLeast();
            int max = ann.atMax();
            return min <= length && length <= max;
        }
    }
}
