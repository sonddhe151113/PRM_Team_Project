package com.example.prm_team_project.Adaptor.Admin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.Domain.ProductDomain;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.Product;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ProductAdaptorAdmin extends RecyclerView.Adapter<ProductAdaptorAdmin.ProductAdaptorAdminHolder>{
    ArrayList<ProductDomain> products;
    public interface OnMyItemClickListener{
        void doSomeThing(int position);
    }
    private OnMyItemClickListener myItemClickListener;

    public void setMyItemClickListener(OnMyItemClickListener myItemClickListener){
        this.myItemClickListener = myItemClickListener;
    }
    public List<String> listProName, listProPic;
    public ProductAdaptorAdmin(List<String> listProName, List<String> listProPic) {
        this.listProName = listProName;
        this.listProPic = listProPic;
    }

    @NonNull
    @NotNull
    @Override
    public ProductAdaptorAdminHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_product, parent, false);
        return new ProductAdaptorAdminHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ProductAdaptorAdminHolder holder, int position) {
        String picUrl = "";
        picUrl = listProPic.get(position);
        int drawableResourceId = holder.itemView.getContext().getResources().getIdentifier(picUrl, "drawable", holder.itemView.getContext().getPackageName());
        holder.imageView.setImageResource(drawableResourceId);
        holder.textView.setText(listProName.get(position));
        holder.btnDelete.setOnClickListener(v -> {
            deleteProduct(listProName.get(position), listProPic.get(position));
        });
        holder.cardView.setOnClickListener(v -> {
            myItemClickListener.doSomeThing(position);
        });
    }

    @Override
    public int getItemCount() {
        return listProName.size();
    }

    class ProductAdaptorAdminHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        private ImageView imageView;
        private TextView textView;
        private Button btnDelete;
        public ProductAdaptorAdminHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view_product);
            imageView = itemView.findViewById(R.id.item_image_view_pro);
            textView = itemView.findViewById(R.id.item_text_view_pro);
            btnDelete = itemView.findViewById(R.id.btn_delete);
        }
    }

    public void addProduct(String name, String pic){
        listProName.add(name);
        listProPic.add(pic);
        notifyDataSetChanged();
    }

    public void addProduct(Product product){
        listProName.add(product.getName());
        listProPic.add(product.getCover());
        notifyDataSetChanged();
    }

    public void editProduct(int position, String name, String pic){
        listProName.set(position, name);
        listProPic.set(position, pic);
        notifyDataSetChanged();
    }
    public void deleteProduct(String name, String pic){
        listProName.remove(name);
        listProPic.remove(pic);
        notifyDataSetChanged();
    }
}