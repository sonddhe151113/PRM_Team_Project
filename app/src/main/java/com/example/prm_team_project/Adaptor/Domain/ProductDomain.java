package com.example.prm_team_project.Adaptor.Domain;

import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.utils.ApiConverters;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@With
public class ProductDomain {
    private String titleProduct;
    private String pic;

    public static ProductDomain of(Product Product) {
        return ProductDomain.builder().titleProduct(Product.getName()).pic(Product.getCover()).build();
    }
}