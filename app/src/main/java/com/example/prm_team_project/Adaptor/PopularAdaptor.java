package com.example.prm_team_project.Adaptor;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.R;
import com.example.prm_team_project.activities.ShowDetailActivity;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.statics.Apps;
import com.squareup.picasso.Picasso;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Getter
@RequiresApi(api = 33)
public class PopularAdaptor extends RecyclerView.Adapter<PopularAdaptor.ViewHolder>{
    List<Product> products;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_popular,parent,false);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = products.get(position);
        holder.foodTitle.setText(product.getName());
        holder.fee.setText(String.valueOf(product.getUnitPrice()));
        String cover = Optional.ofNullable(product.getCover())
                .orElse("https://static.vecteezy.com/system/resources/thumbnails/009/384/620/small/fresh-pizza-and-pizza-box-clipart-design-illustration-free-png.png");
        Picasso.get()
                .load(cover)
                .resize(128,128).noFade().into(holder.foodPic);
        holder.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ShowDetailActivity.class);
                Apps.saveToLocal("productId", product.getId());
                holder.itemView.getContext().startActivity(intent);
            }
        });
//        holder.foodTitle.setText(foods.get(position).getFoodTitle());
//        holder.fee.setText(String.valueOf(foods.get(position).getFee()));
//        int drawableResourceId = holder.itemView.getContext().getResources().getIdentifier(foods.get(position).getFoodPic(), "drawable", holder.itemView.getContext().getPackageName());
//        Glide.with(holder.itemView.getContext())
//                .load(drawableResourceId)
//                .into(holder.foodPic);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView foodTitle, fee;
        ImageView foodPic;
        TextView addBtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            foodTitle = itemView.findViewById(R.id.foodTitle);
            foodPic = itemView.findViewById(R.id.foodPic);
            fee = itemView.findViewById(R.id.fee);
            addBtn = itemView.findViewById(R.id.addBtn);
        }
    }
}
