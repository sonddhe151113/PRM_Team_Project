package com.example.prm_team_project.Adaptor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.Product;
import com.squareup.picasso.Picasso;
import lombok.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class ProductByCategoryAdaptor extends RecyclerView.Adapter<ProductByCategoryAdaptor.ProductByCategoryHolder>{
    List<Product> products;

    @NonNull
    @Override
    public ProductByCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_productbycategory, parent, false);
        return new ProductByCategoryHolder(v);
    }

    @SneakyThrows
    @Override
    public void onBindViewHolder(@NonNull ProductByCategoryHolder holder, int position) {
        Product product = products.get(position);
        Picasso.get()
                .load(product.getCover())
                .resize(50,50).noFade().into(holder.img_product);
        holder.tv_name.setText(product.getName());
        holder.tv_unitproice.setText(String.valueOf(product.getUnitPrice()));
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            Log.e("src", src);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.e("Bitmap", "returned");
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Exception", e.getMessage());
            return null;
        }
    }
    @Override
    public int getItemCount() {
        return products.size();
    }

    class ProductByCategoryHolder extends RecyclerView.ViewHolder {
        ImageView img_product;
        TextView tv_name;
        TextView tv_unitproice;
        public ProductByCategoryHolder(@NonNull View itemView) {
            super(itemView);
            img_product = itemView.findViewById(R.id.imv_product);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_unitproice = itemView.findViewById(R.id.tv_unitproice);
        }
    }
}
