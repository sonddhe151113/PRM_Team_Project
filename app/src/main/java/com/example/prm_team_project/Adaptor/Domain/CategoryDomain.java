package com.example.prm_team_project.Adaptor.Domain;

public class CategoryDomain {
    private String titleCategory;
    private String pic;

    public CategoryDomain(String titleCategory, String pic) {
        this.titleCategory = titleCategory;
        this.pic = pic;
    }

    public String getTitleCategory() {
        return titleCategory;
    }

    public void setTitleCategory(String titleCategory) {
        this.titleCategory = titleCategory;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
