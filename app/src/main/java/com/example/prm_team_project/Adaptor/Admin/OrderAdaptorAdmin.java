package com.example.prm_team_project.Adaptor.Admin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.prm_team_project.Adaptor.Domain.CategoryDomain;
import com.example.prm_team_project.Adaptor.Domain.OrderDomain;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.Order;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class OrderAdaptorAdmin extends RecyclerView.Adapter<OrderAdaptorAdmin.OrderAdaptorAdminHolder>{
    ArrayList<Order> orders;

    public interface OnMyItemClickListener{
        void doSomeThing(int position);
    }
    private OrderAdaptorAdmin.OnMyItemClickListener myItemClickListener;

    public void setMyItemClickListener(OrderAdaptorAdmin.OnMyItemClickListener myItemClickListener){
        this.myItemClickListener = (OnMyItemClickListener) myItemClickListener;
    }
    List<Long> listOrderId = new ArrayList<>();
    List<Long> listProductId = new ArrayList<>();
    List<Double> listUnitPrice = new ArrayList<>();
    List<Long> listQuantity = new ArrayList<>();
    List<Float> listDiscount = new ArrayList<>();
    List<Float> listTotal = new ArrayList<>();
    public OrderAdaptorAdmin(List<Long> listOrderId, List<Long> listProductId, List<Double> listUnitPrice,
                             List<Long> listQuantity, List<Float> listDiscount, List<Float> listTotal) {
        this.listOrderId = listOrderId;
        this.listProductId = listProductId;
        this.listUnitPrice = listUnitPrice;
        this.listQuantity = listQuantity;
        this.listDiscount = listDiscount;
        this.listTotal = listTotal;
    }

    @NonNull
    @NotNull
    @Override
    public OrderAdaptorAdmin.OrderAdaptorAdminHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_layout, parent, false);
        return new OrderAdaptorAdmin.OrderAdaptorAdminHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull OrderAdaptorAdminHolder holder, int position) {
        //holder.txtCustId.setText(list.get(position).toString());
        //holder.txtAddress.setText(listAddress.get(position));
        holder.txtOrderId.setText(listOrderId.get(position).toString());
        holder.txtUnitPrice.setText(listUnitPrice.get(position).toString());
        holder.txtDiscount.setText(listDiscount.get(position).toString());
        holder.txtQuantity.setText(listQuantity.get(position).toString());
        holder.txtProductId.setText(listProductId.get(position).toString());
        holder.txtTotal.setText(listTotal.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return listOrderId.size();
    }

    class OrderAdaptorAdminHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        private TextView txtCustomerId, txtAddress
                , txtUnitPrice, txtOrderId, txtDiscount, txtQuantity, txtTotal, txtProductId;
        public OrderAdaptorAdminHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view_order);
            txtCustomerId = itemView.findViewById(R.id.order_customerId);
            txtAddress = itemView.findViewById(R.id.order_address);
            txtUnitPrice = itemView.findViewById(R.id.order_unitPrice);
            txtOrderId = itemView.findViewById(R.id.order_id);
            txtDiscount = itemView.findViewById(R.id.order_discount);
            txtQuantity = itemView.findViewById(R.id.order_quantity);
            txtTotal = itemView.findViewById(R.id.order_total);
            txtProductId = itemView.findViewById(R.id.order_productId);
        }
    }

}
