package com.example.prm_team_project.Adaptor.Domain;

public class FoodDomain {
    private String foodTitle;
    private String foodPic;
    private String foodDescription;
    private double fee;
    private int numberInCart;

    public FoodDomain() {
    }

    public FoodDomain(String foodTitle, String foodPic, String foodDescription, double fee) {
        this.foodTitle = foodTitle;
        this.foodPic = foodPic;
        this.foodDescription = foodDescription;
        this.fee = fee;
    }

    public String getFoodTitle() {
        return foodTitle;
    }

    public void setFoodTitle(String foodTitle) {
        this.foodTitle = foodTitle;
    }

    public String getFoodPic() {
        return foodPic;
    }

    public void setFoodPic(String foodPic) {
        this.foodPic = foodPic;
    }

    public String getFoodDescription() {
        return foodDescription;
    }

    public void setFoodDescription(String foodDescription) {
        this.foodDescription = foodDescription;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getNumberInCart() {
        return numberInCart;
    }

    public void setNumberInCart(int numberInCart) {
        this.numberInCart = numberInCart;
    }
}
