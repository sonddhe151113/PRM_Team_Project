package com.example.prm_team_project.Adaptor.Domain;

public class OrderDomain {
    private Long customerId;
    private String address;

    public OrderDomain(Long customerId, String address) {
        this.customerId = customerId;
        this.address = address;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long titleCategory) {
        this.customerId = customerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
