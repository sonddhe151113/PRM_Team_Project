package com.example.prm_team_project.Adaptor.Admin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.Domain.CategoryDomain;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.Category;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdaptorAdmin extends RecyclerView.Adapter<CategoryAdaptorAdmin.CategoryAdaptorAdminHolder>{
    public interface OnMyItemClickListener{
        void doSomeThing(int position);
    }
    private OnMyItemClickListener myItemClickListener;

    public void setMyItemClickListener(OnMyItemClickListener myItemClickListener){
        this.myItemClickListener = myItemClickListener;
    }
    public List<String> listCatName, listCatPic;
    public CategoryAdaptorAdmin(List<String> listCatName, List<String> listCatPic) {
        this.listCatName = listCatName;
        this.listCatPic = listCatPic;
    }

    @NonNull
    @NotNull
    @Override
    public CategoryAdaptorAdminHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_category, parent, false);
        return new CategoryAdaptorAdminHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CategoryAdaptorAdminHolder holder, int position) {
        String picUrl = "";
        picUrl = listCatPic.get(position);
        int drawableResourceId = holder.itemView.getContext().getResources().getIdentifier(picUrl, "drawable", holder.itemView.getContext().getPackageName());
        holder.imageView.setImageResource(drawableResourceId);
        holder.textView.setText(listCatName.get(position));
        holder.btnDelete.setOnClickListener(v -> {
            deleteCategory(listCatName.get(position), listCatPic.get(position));
        });
        holder.cardView.setOnClickListener(v -> {
            myItemClickListener.doSomeThing(position);
        });
    }

    @Override
    public int getItemCount() {
        return listCatName.size();
    }

    class CategoryAdaptorAdminHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        private ImageView imageView;
        private TextView textView;
        private Button btnDelete;
        public CategoryAdaptorAdminHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view_category);
            imageView = itemView.findViewById(R.id.item_image_view_cat);
            textView = itemView.findViewById(R.id.item_text_view_cat);
            btnDelete = itemView.findViewById(R.id.btn_delete);
        }
    }

    public void addCategory(String name, String pic){
        listCatName.add(name);
        listCatPic.add(pic);
        notifyDataSetChanged();
    }

    public void addCategory(Category category){
        listCatName.add(category.getName());
        listCatPic.add(category.getImage());
        notifyDataSetChanged();
    }

    public void editCategory(int position, String name, String pic){
        listCatName.set(position, name);
        listCatPic.set(position, pic);
        notifyDataSetChanged();
    }
    public void deleteCategory(String name, String pic){
        listCatName.remove(name);
        listCatPic.remove(pic);
        notifyDataSetChanged();
    }
}
