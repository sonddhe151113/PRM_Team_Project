package com.example.prm_team_project.entities;

import androidx.room.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@With
@Entity(tableName = "products")
public class Product extends BaseEntity {
    private Long categoryId;
    private String name;
    private String cover;
    private String description;
    private Long quantity;
    private Long unitInStock;
    private Double unitPrice;
}
