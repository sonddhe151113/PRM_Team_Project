package com.example.prm_team_project.entities;

import androidx.room.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.With;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@With
@Entity(tableName = "order_details")
public class OrderDetail extends BaseEntity {
    private Long orderId;
    private Long productId;
    private Double unitPrice;
    private Long quantity;
    private Float discount;
}
