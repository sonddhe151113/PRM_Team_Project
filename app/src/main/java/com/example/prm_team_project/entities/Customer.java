package com.example.prm_team_project.entities;

import androidx.room.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.With;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@With
@Entity(tableName = "customers")
public class Customer extends BaseEntity {
    private Long userId;
    private String address;
    private String contactTitle;
    private String contactName;
}
