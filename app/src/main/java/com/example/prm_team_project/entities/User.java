package com.example.prm_team_project.entities;

import androidx.room.Entity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@With
@Entity(tableName = "users")
public class User extends BaseEntity {
    
    @NotNull
    private String email;
    private String username;
    private String avatar;
    
    @NotNull
    private String password;
    private Gender gender;
    private Role role;
    private Status status;
    public enum Gender {
        MALE, FEMALE
    }
    
    public enum Role {
        CUSTOMER, ADMIN
    }
    
    public enum Status {
        ACTIVE, INACTIVE
    }
    
}
