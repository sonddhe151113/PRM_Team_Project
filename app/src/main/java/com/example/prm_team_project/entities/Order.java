package com.example.prm_team_project.entities;

import androidx.room.Entity;
import androidx.room.TypeConverters;
import com.example.prm_team_project.models.converters.TimestampConverter;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.With;
import lombok.experimental.SuperBuilder;

import java.sql.Date;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@With
@Entity(tableName = "orders")
public class Order extends BaseEntity {
    private Long customerId;
    private String address;
    
    @TypeConverters(TimestampConverter.class)
    private Timestamp orderDate;
    
    @TypeConverters(TimestampConverter.class)
    private Timestamp requiredDate;
    
}
