package com.example.prm_team_project.entities;

import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import com.example.prm_team_project.models.converters.TimestampConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.sql.Timestamp;

@SuperBuilder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntity {
    
    @PrimaryKey(autoGenerate = true)
    protected Long id;
    protected Long createdBy;
    
    @TypeConverters(TimestampConverter.class)
    protected Timestamp createdAt;
    
    @TypeConverters(TimestampConverter.class)
    protected Timestamp updatedAt;
    
    @TypeConverters(TimestampConverter.class)
    protected Timestamp deletedAt;
    
}
