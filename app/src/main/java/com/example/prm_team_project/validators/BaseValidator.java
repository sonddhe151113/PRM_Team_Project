package com.example.prm_team_project.validators;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedFieldBy;
import com.example.prm_team_project.statics.Apps;
import com.example.prm_team_project.utils.ApiReflections;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@RequiresApi(api = Build.VERSION_CODES.N)
public abstract class BaseValidator<T> extends Apps {
    
    private final LinkedHashMap<String, String> errors = new LinkedHashMap<>();
    private Object input;
    
    public void reject(String field, Supplier<String> supplier) {
        if (errors.containsKey(field)) {
            return;
        }
        try {
            String value = supplier.get();
            if (StringUtils.isNotBlank(value)) {
                errors.put(field, value);
            }
        } catch (Exception e) {
            errors.put(field, e.getMessage());
        }
    }
    public void rejectIfEmpty(String field, Supplier<String> supplier) {
        if (!errors.isEmpty()) {
            return;
        }
        try {
            String value = supplier.get();
            if (StringUtils.isNotBlank(value)) {
                errors.put(field, value);
            }
        } catch (Exception e) {
            errors.put(field, e.getMessage());
        }
    }
    
    public boolean hasErrors() {
        return !errors.isEmpty();
    }
    
    public Map<String, String> getErrors() {
        return errors;
    }
    
    public void validate(Object input) {
        this.input = input;
        Map<Field, List<Annotation>> annotationMapToField = Lists.newArrayList(input.getClass().getDeclaredFields())
            .stream()
            .collect(Collectors.toMap(field -> field, field -> Lists.newArrayList(field.getAnnotations())));
        
        annotationMapToField.forEach(this::validateField);
        doValidate((T) input);
    }
    
    @RequiresApi(api = 33)
    private void validateField(Field field, List<Annotation> annotations) {
        try {
            annotations
                .forEach(annotation -> {
                    try {
                        ValidatedFieldBy validatorAnnotation = annotation.annotationType().getAnnotation(ValidatedFieldBy.class);
                        if (validatorAnnotation == null) {
                            return;
                        }
                        Constructor<? extends FieldValidator> constructor = validatorAnnotation.value().getConstructor(annotation.annotationType());
                        FieldValidator fieldValidator = constructor.newInstance(annotation);
                        Object value = getFieldValue(field);
                        String message = getMessageOf(annotation);
                        fieldValidator.validate(value, message);
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException ignored) {
                    
                    }
                });
        } catch (Exception e) {
            reject(field.getName(), e::getMessage);
        }
        
    }
    
    private String getMessageOf(final Annotation annotation) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method method = annotation.annotationType().getMethod("message");
        return Optional.ofNullable(method.invoke(annotation)).map(Object::toString).orElse("No message available.");
    }
    
    private Object getFieldValue(Field field) {
        return ApiReflections.getFieldValue(field, input).orElse(null);
    }
    
    protected abstract void doValidate(T target);
    
}
