package com.example.prm_team_project.validators;

import com.example.prm_team_project.models.dtos.ApiException;

import java.lang.annotation.Annotation;

public abstract class FieldValidator<A extends Annotation, T> {
    protected A ann;
    
    public FieldValidator(A ann) {
        this.ann = ann;
    }
    public abstract boolean isValid(T fieldValue);
    
    
    public void validate(Object fieldValue, String message) {
        try {
            if (!isValid((T) fieldValue)) {
                throw ApiException.withMessage(message);
            }
        } catch (ClassCastException e) {
            throw ApiException.withMessage(String.format("ALERT: Wrong type for input with message: %s", message));
        }
  
    }
}
