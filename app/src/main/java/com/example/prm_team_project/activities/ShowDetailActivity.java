package com.example.prm_team_project.activities;

import android.content.Intent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.services.ProductService;
import com.example.prm_team_project.statics.Apps;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.Optional;

@RequiresApi(api = 33)
public class ShowDetailActivity extends AppCompatActivity {
    private LinearLayout btnHome, btnProfile;
    private FloatingActionButton btnCart;
    private TextView addToCartBtn, titleTxt, feeTxt, descriptionTxt, numberOrderTxt;
    private ImageView plusBtn, minusBtn, picFood;
    int numberOrder = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);
        initUI();
        long productId = Apps.getLocalLongValue("productId");
        getProduct(productId);

        btnHome.setOnClickListener(view -> startActivity(new Intent(this, HomeActivity.class)));
//        btnCart.setOnClickListener(view -> startActivity(new Intent(this, CartActivity.class)));
        btnProfile.setOnClickListener(view -> startActivity(new Intent(this, ProfileActivity.class)));
    }

    private void getProduct(long id){
        ProductService productService = new ProductService();
        Product product = productService.findById(id);

        feeTxt.setText("$"+String.valueOf(product.getUnitPrice()));
        titleTxt.setText(product.getName());
        String cover = Optional.ofNullable(product.getCover())
                .orElse("https://static.vecteezy.com/system/resources/thumbnails/009/384/620/small/fresh-pizza-and-pizza-box-clipart-design-illustration-free-png.png");
        Picasso.get()
                .load(cover)
                .resize(128,128).noFade().into(picFood);
//        descriptionTxt.setText(product.get);
        numberOrderTxt.setText(String.valueOf(numberOrder));

        plusBtn.setOnClickListener(view -> {
            numberOrder = numberOrder+1;
            numberOrderTxt.setText(String.valueOf(numberOrder));
        });

        minusBtn.setOnClickListener(view -> {
            if(numberOrder > 1){
                numberOrder=numberOrder-1;
            }
            numberOrderTxt.setText(String.valueOf(numberOrder));
        });
    }

    private void initUI(){
        btnHome = findViewById(R.id.homeBtn);
        btnProfile = findViewById(R.id.profileBtn);
        btnCart = findViewById(R.id.btnCart);
        addToCartBtn = findViewById(R.id.btnAddToCartProductDetail);
        titleTxt = findViewById(R.id.txtTitleProductDetail);
        feeTxt = findViewById(R.id.txtPriceProductDetail);
        descriptionTxt = findViewById(R.id.txDescriptionProductDetail);
        numberOrderTxt = findViewById(R.id.txtNumberOfOrderProductDetail);
        plusBtn = findViewById(R.id.btnPlus);
        minusBtn = findViewById(R.id.btnMinus);
        picFood = findViewById(R.id.imageFood);
    }
}