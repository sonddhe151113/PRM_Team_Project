package com.example.prm_team_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.example.prm_team_project.R;
import com.example.prm_team_project.models.dtos.ValidationException;
import com.example.prm_team_project.models.requests.NewUserRequest;
import com.example.prm_team_project.services.AuthService;

import java.util.Map;

@RequiresApi(api = 33)

public class SignupActivity extends AppCompatActivity {

    EditText edtName, edtEmail, edtPassword, edtRePassword;
    ConstraintLayout btnSignup;
    TextView tvNameError, tvEmailError, tvPassError, tvRePassError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initUi();

        btnSignup.setOnClickListener(v -> signup());

    }

    private void signup() {
        try {
            AuthService authService = new AuthService();
            String name = String.valueOf(edtName.getText());
            String email = String.valueOf(edtEmail.getText());
            String pass = String.valueOf(edtPassword.getText());
            String rePass = String.valueOf(edtRePassword.getText());
            authService.signUp(NewUserRequest.builder().username(name).email(email).password(pass).confirmPassword(rePass).build());
            startActivity(new Intent(SignupActivity.this, SigninActivity.class));
        }catch (Exception e){
            if (e instanceof ValidationException) {
                Map<String, String> errors = ((ValidationException) e).getErrors();
                ((ValidationException) e).getErrors().forEach((error, message) -> {
                    System.out.println(String.format("Error on field %s with message: %s", error, message));
                });
                tvNameError.setText(errors.get("name"));
                tvEmailError.setText(errors.get("email"));
                tvPassError.setText(errors.get("password"));
                return;
            }
            e.printStackTrace();
        }
    }

    private void initUi(){
        edtName = findViewById(R.id.edt_name);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        edtRePassword = findViewById(R.id.edt_repassword);
        btnSignup = findViewById(R.id.btn_signup);
        tvNameError = findViewById(R.id.tv_nameError);
        tvEmailError = findViewById(R.id.tv_emailError);
        tvPassError = findViewById(R.id.tv_passError);
        tvRePassError = findViewById(R.id.tv_rePassError);
    }
}