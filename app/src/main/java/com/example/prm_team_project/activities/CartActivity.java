package com.example.prm_team_project.activities;

import android.content.Intent;
import android.widget.LinearLayout;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.prm_team_project.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

@RequiresApi(api = 33)
public class CartActivity extends AppCompatActivity {
    private LinearLayout btnHome, btnProfile;
    private FloatingActionButton btnCart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        initUI();

        btnHome.setOnClickListener(view -> startActivity(new Intent(this, HomeActivity.class)));
        btnCart.setOnClickListener(view -> startActivity(new Intent(this, CartActivity.class)));
        btnProfile.setOnClickListener(view -> startActivity(new Intent(this, ProfileActivity.class)));
    }

    private void initUI(){
        btnHome = findViewById(R.id.homeBtn);
        btnProfile = findViewById(R.id.profileBtn);
        btnCart = findViewById(R.id.btnCart);
    }
}