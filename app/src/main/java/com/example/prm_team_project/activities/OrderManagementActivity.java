package com.example.prm_team_project.activities;

import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.OrderDetail;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.Admin.CategoryAdaptorAdmin;
import com.example.prm_team_project.Adaptor.Admin.OrderAdaptorAdmin;
import com.example.prm_team_project.Adaptor.CategoryAdaptor;
import com.example.prm_team_project.Adaptor.Domain.CategoryDomain;
import com.example.prm_team_project.Adaptor.Domain.FoodDomain;
import com.example.prm_team_project.Adaptor.Domain.OrderDomain;
import com.example.prm_team_project.Adaptor.PopularAdaptor;
import com.example.prm_team_project.Adaptor.ProductByCategoryAdaptor;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.entities.OrderDetail;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.models.requests.NewCategoryRequest;
import com.example.prm_team_project.models.requests.UpdateCategoryRequest;
import com.example.prm_team_project.services.CategoryService;
import com.example.prm_team_project.statics.Apps;
import java.util.ArrayList;
import java.util.List;

public class OrderManagementActivity extends AppCompatActivity{
    private RecyclerView.Adapter adapter;
    private RecyclerView.Adapter adapter2;
    private RecyclerView recyclerViewCategoryList;
    private RecyclerView recyclerViewPopularList;
    private RecyclerView recyclerViewOrderList;

    private ConstraintLayout btnStart;

    private CategoryAdaptorAdmin categoryAdaptorAdmin;
    private OrderAdaptorAdmin orderAdaptorAdmin;

    private EditText editTextCatName, editTextCatPic;
    private TextView txtOrderCusId, txtOrderAddress;
    private Button btnAdd, btnEdit, btnLoad, btnView;
    private int currentPosition;
    @RequiresApi(api = 33)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_order);

        Apps.initRepo(this);
        Apps.initLocalStorage(this);

        ArrayList<OrderDetail> orders = new ArrayList<OrderDetail>();
        //CategoryService service = new CategoryService();
        //List<CategoryDomain> categoryDomains = service.findAll().stream().map(x -> CategoryDomain.of(x)).collect(Collectors.toList());
        orders.add(new OrderDetail(1L, 1L, 100.4, 2L, 0f));
        orders.add(new OrderDetail(2L, 2L, 200.4, 2L, 0f));
        orders.add(new OrderDetail(3L, 3L, 300.4, 2L, 0f));
        orders.add(new OrderDetail(4L, 4L, 400.4, 2L, 0f));
        orders.add(new OrderDetail(5L, 5L, 500.4, 2L, 0f));

        List<Long> listOrderId = new ArrayList<>();
        List<Long> listProductId = new ArrayList<>();
        List<Double> listUnitPrice = new ArrayList<>();
        List<Long> listQuantity = new ArrayList<>();
        List<Float> listDiscount = new ArrayList<>();
        List<Float> listTotal = new ArrayList<>();
        for(OrderDetail order: orders){
            float total = (float) (order.getQuantity() * order.getUnitPrice());
            listOrderId.add(order.getOrderId());
            listProductId.add(order.getProductId());
            listUnitPrice.add(order.getUnitPrice());
            listQuantity.add(order.getQuantity());
            listDiscount.add(order.getDiscount());
            listTotal.add(total);
        }
        initViewOrder();
        orderAdaptorAdmin = new OrderAdaptorAdmin(listOrderId, listProductId, listUnitPrice,
                listQuantity, listDiscount, listTotal);
        recyclerViewOrderList.setAdapter(orderAdaptorAdmin);
        recyclerViewOrderList.setLayoutManager(new GridLayoutManager(this, 1));

    }
    private void initViewOrder(){
        recyclerViewOrderList = findViewById(R.id.recycler_view_order);
        txtOrderCusId = findViewById(R.id.order_customerId);
        txtOrderAddress = findViewById(R.id.order_address);
    }
}
