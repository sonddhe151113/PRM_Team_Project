package com.example.prm_team_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.CategoryAdaptor;
import com.example.prm_team_project.Adaptor.PopularAdaptor;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.services.AuthService;
import com.example.prm_team_project.services.CategoryService;
import com.example.prm_team_project.services.ProductService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

@RequiresApi(api = 33)
public class HomeActivity extends AppCompatActivity {
    private RecyclerView.Adapter adapterFood, adapterCategory;
    private RecyclerView recyclerViewCategoryList, recyclerViewPopulateList;
    private ImageView profileImage;
    private LinearLayout btnHome, btnProfile;
    private FloatingActionButton btnCart;
    private TextView tvName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);;
        initUI();
//        setProfile();
        recyclerViewCategory();
        recyclerViewProduct();

        AuthService authService = new AuthService();
        User currentUser = authService.getCurrentUser();
        if(currentUser != null){
            tvName.setText(currentUser.getUsername());
            //        String cover = Optional.ofNullable(currentUser.getAvatar())
//                .orElse("https://cdn-icons-png.flaticon.com/512/2688/2688584.png");
//        Picasso.get().load(cover).resize(128,128).noFade().into((Target) btnProfile);
        }else {
            tvName.setVisibility(View.GONE);
            btnProfile.setVisibility(View.GONE);
        }

        profileImage.setOnClickListener(view -> startActivity(new Intent(this, ProfileActivity.class)));
        btnHome.setOnClickListener(view -> startActivity(new Intent(this, HomeActivity.class)));
        btnCart.setOnClickListener(view -> startActivity(new Intent(this, CartActivity.class)));
        btnProfile.setOnClickListener(view -> startActivity(new Intent(this, ProfileActivity.class)));
    }

    private void setProfile(User cur){
        if(cur != null){
            tvName.setText(cur.getUsername());
            //        String cover = Optional.ofNullable(currentUser.getAvatar())
//                .orElse("https://cdn-icons-png.flaticon.com/512/2688/2688584.png");
//        Picasso.get().load(cover).resize(128,128).noFade().into((Target) btnProfile);
        }else {
            tvName.setVisibility(View.GONE);
            btnProfile.setVisibility(View.GONE);
        }
    }

    private void recyclerViewProduct() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewPopulateList.setLayoutManager(linearLayoutManager);
        ProductService productService = new ProductService();
        List<Product> listProduct = productService.findAll();

        adapterFood = new PopularAdaptor(listProduct);
        recyclerViewPopulateList.setAdapter(adapterFood);
    }

    private void recyclerViewCategory() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewCategoryList.setLayoutManager(linearLayoutManager);
        CategoryService categoryService = new CategoryService();
        List<Category> listCategory = categoryService.findAll();

        adapterCategory = new CategoryAdaptor(listCategory);
        recyclerViewCategoryList.setAdapter(adapterCategory);
    }

    private void initUI(){
        recyclerViewCategoryList = findViewById(R.id.recyclerViewCategory);
        recyclerViewPopulateList = findViewById(R.id.recyclerViewFood);
        profileImage = findViewById(R.id.imageProfile);
        btnHome = findViewById(R.id.homeBtn);
        btnProfile = findViewById(R.id.profileBtn);
        btnCart = findViewById(R.id.btnCart);
        tvName = findViewById(R.id.tvName);
    }
}