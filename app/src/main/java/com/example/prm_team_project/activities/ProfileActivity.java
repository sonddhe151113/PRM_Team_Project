package com.example.prm_team_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import com.example.prm_team_project.R;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.models.requests.NewCustomerRequest;
import com.example.prm_team_project.services.AuthService;
import com.example.prm_team_project.services.CustomerService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.Optional;

@RequiresApi(api = 33)
public class ProfileActivity extends AppCompatActivity {
    private LinearLayout btnHome, btnProfile;
    private FloatingActionButton btnCart;
    private TextView txtUsernameBig;
    private TextView txtUsername;
    private TextView txtEmail;
    private TextView txtContactTitle;
    private TextView txtContactName;
    private TextView txtAddress;
    private ImageView imgAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initUI();
        
        btnHome.setOnClickListener(view -> startActivity(new Intent(this, HomeActivity.class)));
        btnCart.setOnClickListener(view -> startActivity(new Intent(this, CartActivity.class)));
        btnProfile.setOnClickListener(view -> startActivity(new Intent(this, ProfileActivity.class)));
    
        User user = AuthService.getCurrentUser();
        txtUsernameBig.setText(user.getUsername());
        txtUsername.setText(user.getUsername());
        txtEmail.setText(user.getEmail());
        
        CustomerService customerService = new CustomerService();
        customerService.addCustomer(NewCustomerRequest.builder().contactName("Demo contact name").contactTitle("Demo contact title").userId(user.getId()).address("HoaLac, HaNoi").build());
        String avatar = Optional.ofNullable(user.getAvatar()).orElse("https://img.icons8.com/color/480/circled-user-male-skin-type-7--v1.png");
        Picasso.get().load(avatar).resize(128,128).noFade().into(imgAvatar);
        
        customerService.getByUserId(user.getId()).ifPresent(customer -> {
            txtAddress.setText(customer.getAddress());
            txtContactName.setText(customer.getContactName());
            txtContactTitle.setText(customer.getContactTitle());
        });
        
    }

    private void initUI(){
        btnHome = findViewById(R.id.homeBtn);
        btnProfile = findViewById(R.id.profileBtn);
        btnCart = findViewById(R.id.btnCart);
        
        txtUsernameBig = findViewById(R.id.textView6);
        txtUsername = findViewById(R.id.txt_username);
        txtEmail = findViewById(R.id.textView9);
        txtContactTitle = findViewById(R.id.txt_contact_title);
        txtContactName = findViewById(R.id.txt_contact_name);
        txtAddress = findViewById(R.id.txt_address);
        imgAvatar = findViewById(R.id.imageView2);
    }
}