package com.example.prm_team_project.activities;

import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.Admin.ProductAdaptorAdmin;
import com.example.prm_team_project.Adaptor.Domain.ProductDomain;
import com.example.prm_team_project.Adaptor.ProductByCategoryAdaptor;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.models.requests.NewCategoryRequest;
import com.example.prm_team_project.models.requests.UpdateCategoryRequest;
import com.example.prm_team_project.services.CategoryService;
import com.example.prm_team_project.statics.Apps;

import java.util.ArrayList;
import java.util.List;
import com.example.prm_team_project.R;




public class ProductManagementActivity extends AppCompatActivity {
    private RecyclerView recyclerViewProductList;
    private ProductAdaptorAdmin productAdaptorAdmin;

    private EditText editTextProName, editTextProPic;
    private Button btnAdd, btnEdit, btnLoad;
    private int currentPosition;

    @RequiresApi(api = 33)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_product);

        Apps.initRepo(this);
        Apps.initLocalStorage(this);
        ArrayList<ProductDomain> products = new ArrayList<ProductDomain>();
        //CategoryService service = new CategoryService();
        //List<CategoryDomain> categoryDomains = service.findAll().stream().map(x -> CategoryDomain.of(x)).collect(Collectors.toList());
        products.add(new ProductDomain("Pizza", "cat_1"));
        products.add(new ProductDomain("Burger", "cat_2"));
        products.add(new ProductDomain("Hotdog", "cat_3"));
        products.add(new ProductDomain("Drink", "cat_4"));
        products.add(new ProductDomain("Donut", "cat_5"));

        List<String> listProductName = new ArrayList<>();
        List<String> listProductPic = new ArrayList<>();
        for(ProductDomain productDomain: products){
            listProductName.add(productDomain.getTitleProduct());
            listProductPic.add(productDomain.getPic());
        }
        initView();
        productAdaptorAdmin = new ProductAdaptorAdmin(listProductName, listProductPic);
        productAdaptorAdmin.setMyItemClickListener(new ProductAdaptorAdmin.OnMyItemClickListener() {
            @Override
            public void doSomeThing(int position) {
                currentPosition = position;
                editTextProName.setText(productAdaptorAdmin.listProName.get(position));
                editTextProPic.setText(productAdaptorAdmin.listProPic.get(position));
                toggleEditButton();
            }
        });
        recyclerViewProductList.setAdapter(productAdaptorAdmin);
        recyclerViewProductList.setLayoutManager(new GridLayoutManager(this, 1));

        handleAddClick();
        handleEditClick();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Apps.autoSyncData(() -> {
//                setContentView(R.layout.activity_productbycategory);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                }
//            });
//        }
//        testOfBackEndDoNotComment();
//        setContentView(R.layout.activity_splash);
//        btnStart = findViewById(R.id.btn_start);
//        btnStart.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, SigninActivity.class)));
//
//        RecyclerViewProductByCategory();
    }
    private void toggleEditButton(){
        btnAdd.setEnabled(false);
        btnEdit.setEnabled(true);
    }
    private void toggleAddButton(){
        btnAdd.setEnabled(true);
        btnEdit.setEnabled(false);
    }
    private void initView(){
        recyclerViewProductList = findViewById(R.id.recycler_view_product);
        editTextProName = findViewById(R.id.edit_text_ProductName);
        editTextProPic = findViewById(R.id.edit_text_ProductImage);
        btnAdd = findViewById(R.id.btn_Add);
        btnEdit = findViewById(R.id.btn_Edit);
    }
    private void handleLoadClick(){
        btnLoad.setOnClickListener(v -> {

        });
    }
    private void handleEditClick(){
        btnEdit.setOnClickListener(v -> {
            try{
                ProductDomain productDomain = packDataFromForm();
                //UpdateCategoryRequest request = UpdateCategoryRequest.builder().categoryId((long)currentPosition).name(categoryDomain.getTitleCategory()).build();
                //CategoryService categoryService = new CategoryService();
                //categoryService.updateCategory(request, null);
                productAdaptorAdmin.editProduct(currentPosition, productDomain.getTitleProduct(), productDomain.getPic());
                clearForm();
                toggleAddButton();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }
    private void handleAddClick(){
        btnAdd.setOnClickListener(v -> {
            try{
                ProductDomain productDomain = packDataFromForm();
                //NewCategoryRequest request = NewCategoryRequest.builder().name(categoryDomain.getTitleCategory()).build();
                //CategoryService categoryService = new CategoryService();
                //Category category = categoryService.addCategory(request, null);
                //CategoryDomain domain = CategoryDomain.of(category);
                productAdaptorAdmin.addProduct(productDomain.getTitleProduct(), productDomain.getPic());
                clearForm();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }
    private void clearForm(){
        editTextProPic.setText("");
        editTextProName.setText("");
    }

    @NonNull
    private ProductDomain packDataFromForm() {
        String proName = editTextProName.getText().toString();
        String proPic = editTextProPic.getText().toString();
        ProductDomain categoryDomain = new ProductDomain(proName, proPic);
        return categoryDomain;
    }
}
