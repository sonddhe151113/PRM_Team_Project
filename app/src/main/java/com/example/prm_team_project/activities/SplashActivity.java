package com.example.prm_team_project.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.prm_team_project.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}