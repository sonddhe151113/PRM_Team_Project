package com.example.prm_team_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.example.prm_team_project.R;
import com.example.prm_team_project.models.dtos.ValidationException;
import com.example.prm_team_project.models.requests.SignInRequest;
import com.example.prm_team_project.services.AuthService;

import java.util.Map;

//@RequiresApi(api = Build.VERSION_CODES.N)
@RequiresApi(api = 33)
public class SigninActivity extends AppCompatActivity {
    EditText edtEmail, edtPassword;
    ConstraintLayout btnLogin;
    TextView tvSignup, tvForgot, tvEmailError, tvPassError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        initUi();

        btnLogin = findViewById(R.id.btn_signin);

        tvSignup.setOnClickListener(view -> startActivity(new Intent(SigninActivity.this, SignupActivity.class)));

        btnLogin.setOnClickListener(view -> login());
    }


    private void login() {
        try {
            AuthService authService = new AuthService();
            String email = String.valueOf(edtEmail.getText());
            String pass = String.valueOf(edtPassword.getText());
            authService.signIn(SignInRequest.builder().email(email).password(pass).build());

            if(AuthService.getCurrentUser().getRole().equals("ADMIN")){
                startActivity(new Intent(SigninActivity.this, DashboardActivity.class));
            }else{
                startActivity(new Intent(SigninActivity.this, HomeActivity.class));
            }
        }catch (Exception e){
            if (e instanceof ValidationException) {
                Map<String, String> errors = ((ValidationException) e).getErrors();
                ((ValidationException) e).getErrors().forEach((error, message) -> {
                        System.out.println(String.format("Error on field %s with message: %s", error, message));
                    });
                tvEmailError.setText(errors.get("email"));
                tvPassError.setText(errors.get("password"));
                return;
            }
            e.getMessage();
            e.printStackTrace();
        }
    }

    private void initUi(){
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnLogin = findViewById(R.id.btn_signin);
        tvSignup = findViewById(R.id.tv_signup);
        tvForgot = findViewById(R.id.tv_forgot);
        tvEmailError = findViewById(R.id.tv_emailError);
        tvPassError = findViewById(R.id.tv_passError);
    }
}