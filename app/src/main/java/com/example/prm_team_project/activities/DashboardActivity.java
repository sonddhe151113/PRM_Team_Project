package com.example.prm_team_project.activities;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import com.example.prm_team_project.R;

public class DashboardActivity extends AppCompatActivity{
    Button btnCategory, btnOrder, btnProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initUi();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            btnCategory.setOnClickListener(v -> categoryManagement());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            btnOrder.setOnClickListener(v -> orderManagement());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            btnProduct.setOnClickListener(v -> productManagement());
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void categoryManagement() {
        try {
            startActivity(new Intent(DashboardActivity.this, CategoryManagementActivity.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void orderManagement() {
        try {
            startActivity(new Intent(DashboardActivity.this, CategoryManagementActivity.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void productManagement() {
        try {
            startActivity(new Intent(DashboardActivity.this, CategoryManagementActivity.class));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initUi(){
        btnProduct = findViewById(R.id.btn_Admin_Product);
        btnCategory = findViewById(R.id.btn_Admin_Category);
        btnOrder = findViewById(R.id.btn_Admin_Order);
    }
}
