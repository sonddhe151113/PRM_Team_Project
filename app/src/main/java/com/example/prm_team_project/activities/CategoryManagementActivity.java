package com.example.prm_team_project.activities;

import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.prm_team_project.Adaptor.Admin.CategoryAdaptorAdmin;
import com.example.prm_team_project.Adaptor.Domain.CategoryDomain;
import com.example.prm_team_project.Adaptor.ProductByCategoryAdaptor;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.models.requests.NewCategoryRequest;
import com.example.prm_team_project.models.requests.UpdateCategoryRequest;
import com.example.prm_team_project.services.CategoryService;
import com.example.prm_team_project.statics.Apps;

import java.util.ArrayList;
import java.util.List;
import com.example.prm_team_project.R;




public class CategoryManagementActivity extends AppCompatActivity {
    private RecyclerView recyclerViewCategoryList;
    private CategoryAdaptorAdmin categoryAdaptorAdmin;

    private EditText editTextCatName, editTextCatPic;
    private Button btnAdd, btnEdit, btnLoad;
    private int currentPosition;

    @RequiresApi(api = 33)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_category);

        Apps.initRepo(this);
        Apps.initLocalStorage(this);
        ArrayList<CategoryDomain> categories = new ArrayList<CategoryDomain>();
        //CategoryService service = new CategoryService();
        //List<CategoryDomain> categoryDomains = service.findAll().stream().map(x -> CategoryDomain.of(x)).collect(Collectors.toList());
        categories.add(new CategoryDomain("Pizza", "cat_1"));
        categories.add(new CategoryDomain("Burger", "cat_2"));
        categories.add(new CategoryDomain("Hotdog", "cat_3"));
        categories.add(new CategoryDomain("Drink", "cat_4"));
        categories.add(new CategoryDomain("Donut", "cat_5"));

        List<String> listCategoryName = new ArrayList<>();
        List<String> listCategoryPic = new ArrayList<>();
        for(CategoryDomain categoryDomain: categories){
            listCategoryName.add(categoryDomain.getTitleCategory());
            listCategoryPic.add(categoryDomain.getPic());
        }
        initView();
        categoryAdaptorAdmin = new CategoryAdaptorAdmin(listCategoryName, listCategoryPic);
        categoryAdaptorAdmin.setMyItemClickListener(new CategoryAdaptorAdmin.OnMyItemClickListener() {
            @Override
            public void doSomeThing(int position) {
                currentPosition = position;
                editTextCatName.setText(categoryAdaptorAdmin.listCatName.get(position));
                editTextCatPic.setText(categoryAdaptorAdmin.listCatPic.get(position));
                toggleEditButton();
            }
        });
        recyclerViewCategoryList.setAdapter(categoryAdaptorAdmin);
        recyclerViewCategoryList.setLayoutManager(new GridLayoutManager(this, 1));

        handleAddClick();
        handleEditClick();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Apps.autoSyncData(() -> {
//                setContentView(R.layout.activity_productbycategory);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                }
//            });
//        }
//        testOfBackEndDoNotComment();
//        setContentView(R.layout.activity_splash);
//        btnStart = findViewById(R.id.btn_start);
//        btnStart.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, SigninActivity.class)));
//
//        RecyclerViewProductByCategory();
    }
    private void toggleEditButton(){
        btnAdd.setEnabled(false);
        btnEdit.setEnabled(true);
    }
    private void toggleAddButton(){
        btnAdd.setEnabled(true);
        btnEdit.setEnabled(false);
    }
    private void initView(){
        recyclerViewCategoryList = findViewById(R.id.recycler_view_category);
        editTextCatName = findViewById(R.id.edit_text_CategoryName);
        editTextCatPic = findViewById(R.id.edit_text_CategoryImage);
        btnAdd = findViewById(R.id.btn_Add);
        btnEdit = findViewById(R.id.btn_Edit);
    }
    private void handleLoadClick(){
        btnLoad.setOnClickListener(v -> {

        });
    }
    private void handleEditClick(){
        btnEdit.setOnClickListener(v -> {
            try{
                CategoryDomain categoryDomain = packDataFromForm();
                //UpdateCategoryRequest request = UpdateCategoryRequest.builder().categoryId((long)currentPosition).name(categoryDomain.getTitleCategory()).build();
                //CategoryService categoryService = new CategoryService();
                //categoryService.updateCategory(request, null);
                categoryAdaptorAdmin.editCategory(currentPosition, categoryDomain.getTitleCategory(), categoryDomain.getPic());
                clearForm();
                toggleAddButton();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }
    private void handleAddClick(){
        btnAdd.setOnClickListener(v -> {
            try{
                CategoryDomain categoryDomain = packDataFromForm();
                //NewCategoryRequest request = NewCategoryRequest.builder().name(categoryDomain.getTitleCategory()).build();
                //CategoryService categoryService = new CategoryService();
                //Category category = categoryService.addCategory(request, null);
                //CategoryDomain domain = CategoryDomain.of(category);
                categoryAdaptorAdmin.addCategory(categoryDomain.getTitleCategory(), categoryDomain.getPic());
                clearForm();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }
    private void clearForm(){
        editTextCatPic.setText("");
        editTextCatName.setText("");
    }

    @NonNull
    private CategoryDomain packDataFromForm() {
        String catName = editTextCatName.getText().toString();
        String catPic = editTextCatPic.getText().toString();
        CategoryDomain categoryDomain = new CategoryDomain(catName, catPic);
        return categoryDomain;
    }
}
