package com.example.prm_team_project.statics;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.configs.DatabaseConfig;
import com.example.prm_team_project.models.dtos.ApiException;
import com.example.prm_team_project.models.dtos.Cart;
import com.example.prm_team_project.repositories.*;
import com.example.prm_team_project.services.UploadService;
import com.example.prm_team_project.utils.ApiConverters;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Date;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

@RequiresApi(api = 33)
public class Apps {
    public static final String CART_KEY = "Cart";
    public static CategoryRepository categoryRepository;
    public static CustomerRepository customerRepository;
    public static OrderRepository orderRepository;
    public static OrderDetailRepository orderDetailRepository;
    public static UserRepository userRepository;
    public static ProductRepository productRepository;
    public static UploadService uploadService = new UploadService();
    
    public static SharedPreferences storage;
    
    public static boolean synchronizedData;
    
    public static void initRepo(Context context) {
        DatabaseConfig db = DatabaseConfig.getInstance(context);
        categoryRepository = db.categoryRepository();
        customerRepository = db.customerRepository();
        orderRepository = db.orderRepository();
        orderDetailRepository = db.orderDetailRepository();
        userRepository = db.userRepository();
        productRepository = db.productRepository();
        uploadService.initStorage(context);
    }
    
//    public static void syncDataToFirebase() {
//        categoryRepository.syncDataToFirebase();
//        customerRepository.syncDataToFirebase();
//        orderRepository.syncDataToFirebase();
//        orderDetailRepository.syncDataToFirebase();
//        userRepository.syncDataToFirebase();
//        productRepository.syncDataToFirebase();
//    }
//
    public static boolean isSynchronizedData() {
        return synchronizedData;
    }
    
    public static void syncDataFromFirebase(final Runnable onSuccessAction) {
        categoryRepository.syncDataFromFirebase();
        customerRepository.syncDataFromFirebase();
        orderRepository.syncDataFromFirebase();
        orderDetailRepository.syncDataFromFirebase();
        userRepository.syncDataFromFirebase();
        productRepository.syncDataFromFirebase(onSuccessAction);
    }
    public static void syncDataFromFirebase() {
        syncDataFromFirebase(null);
    }

    public static void initLocalStorage(Context context) {
        storage = context.getApplicationContext().getSharedPreferences("mySettings", 0);
    }

    public static void autoSyncData(Runnable onSuccessAction) {
        java.util.Date now = Date.from(Instant.now());
        Timer timer = new Timer();
        
        int period = 30 * 60 * 1000;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!Apps.isSynchronizedData()) {
                    syncDataFromFirebase(onSuccessAction);
                    return;
                }
                syncDataFromFirebase();
            }
        }, now, period);
    }
    
    public static void saveToLocal(@NotNull String key, @Nullable Object value) {
        SharedPreferences.Editor editor = storage.edit();
        putValue(key, value, editor);
        editor.apply();
    }
    
    
    private static void putValue(final @NotNull String key, final Object value, final SharedPreferences.Editor editor) {
        if (value instanceof Integer) {
            editor.putInt(key, ((Integer) value));
            return;
        }
        if (value instanceof String) {
            editor.putString(key, ((String) value));
            return;
        }
        if (value instanceof Long) {
            editor.putLong(key, ((Long) value));
            return;
        }
        if (value instanceof Boolean) {
            editor.putBoolean(key, ((Boolean) value));
            return;
        }
        if (value instanceof Float) {
            editor.putFloat(key, ((Float) value));
            return;
        }
        String json = ApiConverters.toJson(value);
        editor.putString(key, json);
    }
    
    public static Integer getLocalIntValue(@NotNull String key, Integer defaultValue) {
        try {
            return storage.getInt(key, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }
    public static Integer getLocalIntValue(@NotNull String key) {
        return getLocalIntValue(key, null);
    }
    
    public static String getLocalValue(@NotNull String key, String defaultValue) {
        return storage.getString(key, defaultValue);
    }
    
    public static String getLocalValue(@NotNull String key) {
        return getLocalValue(key, null);
    }
    
    public static Long getLocalLongValue(@NotNull String key, Long defaultValue) {
        try {
            Map<String, ?> values = storage.getAll();
            return (Long) values.get(key);
        } catch (Exception e) {
            return defaultValue;
        }
    }
    
    public static Long getLocalLongValue(@NotNull String key) {
        return getLocalLongValue(key, null);
    }
    
    public static <T> T getLocalObjectValue(@NotNull String key, @NotNull Class<T> clazz) {
        String json = getLocalValue(key, null);
        return ApiConverters.toObject(json, clazz);
    }
    
    @NotNull
    public static Cart getCart() {
        return Optional.ofNullable(getLocalObjectValue(CART_KEY, Cart.class)).orElseGet(Cart::new);
    }
    
    public static void saveCart(Cart cart) {
        saveToLocal(CART_KEY, cart);
    }
    
    public static void checkSyncData() {
        if (!isSynchronizedData()) {
            throw ApiException.withMessage("You have not synchronized data. Can not continue");
        }
    }
}
