package com.example.prm_team_project.configs;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.entities.Customer;
import com.example.prm_team_project.entities.Order;
import com.example.prm_team_project.entities.OrderDetail;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.repositories.CategoryRepository;
import com.example.prm_team_project.repositories.CustomerRepository;
import com.example.prm_team_project.repositories.OrderDetailRepository;
import com.example.prm_team_project.repositories.OrderRepository;
import com.example.prm_team_project.repositories.ProductRepository;
import com.example.prm_team_project.repositories.UserRepository;

@Database(entities = {
    Category.class,
    Customer.class,
    Order.class,
    OrderDetail.class,
    Product.class,
    User.class,
}, version = 1)
public abstract class DatabaseConfig extends RoomDatabase {

    private static final String DATABASE_NAME = "prm.db";
    private static DatabaseConfig instance;

    public static DatabaseConfig getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), DatabaseConfig.class, DATABASE_NAME).allowMainThreadQueries().build();
        }
        return instance;
    }

    public abstract CategoryRepository categoryRepository();
    public abstract CustomerRepository customerRepository();
    public abstract OrderDetailRepository orderDetailRepository();
    public abstract OrderRepository orderRepository();
    public abstract ProductRepository productRepository();
    public abstract UserRepository userRepository();
}
