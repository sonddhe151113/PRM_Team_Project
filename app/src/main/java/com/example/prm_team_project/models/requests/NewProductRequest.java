package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.NumberShouldBe;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.annotations.input.ShouldNotNull;
import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.utils.ApiConverters;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@RequiresApi(api = Build.VERSION_CODES.N)
@ValidatedBy(NewProductRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class NewProductRequest extends BaseAddRequest {
    
    @ShouldNotNull(message = "category id must not be null")
    private Long categoryId;
    
    @ShouldNotBlank(message = "name should not be blank")
    private String name;
    
    private String description;
    
    @NumberShouldBe(largerThan = 0, message = "quantity must be larger than 0")
    private Long quantity;
    
    @NumberShouldBe(largerThan = 0, message = "unit in stock must be larger than 0")
    private Long unitInStock;
    
    @NumberShouldBe(largerThan = 0, message = "unit price must be larger than 0")
    private Double unitPrice;
    
    public Product toProduct() {
        return ApiConverters.convertTo(Product.class, this);
    }
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<NewProductRequest> {
    
        @Override
        protected void doValidate(NewProductRequest target) {
            reject("categoryId", () -> validateCategoryId(target.getCategoryId()));
        }
    
        private String validateCategoryId(final @Nullable Long categoryId) {
            if (categoryRepository.getOne(categoryId) == null) {
                return "categoryId not found";
            }
            return null;
        }
    }
}
