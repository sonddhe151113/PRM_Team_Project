package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldNotNull;
import com.example.prm_team_project.annotations.input.UpdateAble;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.utils.ApiConverters;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@RequiresApi(api = Build.VERSION_CODES.N)
@ValidatedBy(UpdateProfileRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class UpdateProfileRequest extends BaseUpdateRequest {
    
    @ShouldNotNull(message = "user id must not be null")
    private Long userId;
    
    @ShouldNotNull(message = "username must not be null")
    @UpdateAble
    private String username;
    
    @UpdateAble
    private User.Gender gender;
    
    @RequiresApi(api = 33)
    public User updateFor(User user) {
        return ApiConverters.updateFor(user, this);
    }
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<UpdateProfileRequest> {
        
        @Override
        protected void doValidate(UpdateProfileRequest target) {
        }
        
    }
}
