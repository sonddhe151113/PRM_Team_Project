package com.example.prm_team_project.models.requests;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.annotations.input.ShouldNotNull;
import com.example.prm_team_project.annotations.input.UpdateAble;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.utils.ApiConverters;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@ValidatedBy(UpdateCategoryRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@RequiresApi(api = 33)
public class UpdateCategoryRequest extends BaseUpdateRequest {
    
    @ShouldNotNull(message = "id should not be null")
    private Long categoryId;
    
    @UpdateAble
    @ShouldNotBlank(message = "name should not be blank")
    private String name;
    
    @UpdateAble
    private String description;
    
    public Category updateFor(Category src) {
        return ApiConverters.updateFor(src, this);
    }
    
    @Builder
    @Getter
    @RequiresApi(api = 33)
    @NoArgsConstructor
    public static class Validator extends BaseValidator<UpdateCategoryRequest> {
    
        @Override
        protected void doValidate(UpdateCategoryRequest target) {
            reject("name", () -> validateName(target.getCategoryId() , target.getName()));
        }
    
        private String validateName(final Long categoryId, final String name) {
            Category category = categoryRepository.findOneOrThrow(categoryId, "Category not found");
            if (category.getName().equalsIgnoreCase(name)) {
                return null;
            }
            if (categoryRepository.findOneByField("name", name).isPresent()) {
                return "Name already existed.";
            }
            return null;
        }
    }
}
