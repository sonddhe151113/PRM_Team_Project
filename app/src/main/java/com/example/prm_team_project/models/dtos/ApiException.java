package com.example.prm_team_project.models.dtos;

public class ApiException extends RuntimeException {
    public ApiException(String message) {
        super(message);
    }
    
    public static ApiException withMessage(String message) {
        return new ApiException(message);
    }
}
