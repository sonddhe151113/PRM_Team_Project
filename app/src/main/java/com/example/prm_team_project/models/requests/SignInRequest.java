package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.utils.ApiStrings;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@ValidatedBy(SignInRequest.Validator.class)
@Getter
@Setter
@AllArgsConstructor
@Builder
@RequiresApi(api = Build.VERSION_CODES.N)
public class SignInRequest extends BaseRequest {
    private String email;
    private String password;
    
    @Builder
    @Getter
    @NoArgsConstructor
    public static class Validator extends BaseValidator<SignInRequest> {
    
        @Override
        protected void doValidate(SignInRequest target) {
            reject("email", () -> validateEmail(target.getEmail()));
            rejectIfEmpty("password", () -> validatePassword(target.getEmail(), target.getPassword()));
        }
    
        private String validateEmail(final @Nullable String email) {
            if (!Objects.requireNonNull(email).contains("@")) {
                return "Invalid email";
            }
            if (!userRepository.findOne(email).isPresent()) {
                return "Email not found.";
            }
            return null;
        }
    
        private String validatePassword(final String email, final String password) {
            User user = userRepository.findOne(email).orElse(null);
            if (!ApiStrings.checkPassword(password, user.getPassword())) {
                return "Wrong password";
            }
            
            return null;
        }
    }
    
}
