package com.example.prm_team_project.models.dtos;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class ValidationException extends RuntimeException {
    
    private Map<String, String> errors;
    
    public static ValidationException withErrors(Map<String, String> errors) {
        return new ValidationException(errors);
    }
}
