package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldHaveLength;
import com.example.prm_team_project.annotations.input.ShouldMeetPattern;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@RequiresApi(api = Build.VERSION_CODES.N)
@ValidatedBy(NewUserRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class NewUserRequest extends BaseAddRequest {
    
    @ShouldNotBlank(message = "Please enter your username")
    private String username;
    
    @ShouldNotBlank(message = "Please enter your email")
    private String email;
    
    @ShouldHaveLength(atLeast = 8, atMax = 32, message = "Password must have 8-32 characters")
    @ShouldMeetPattern(value = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", message = "Password must contains at least 1 digit, 1 uppercase letter, 1 lowercase letter")
    private String password;
    
    @ShouldHaveLength(atLeast = 8, atMax = 32, message = "Confirm password must have 8-32 characters")
    @ShouldMeetPattern(value = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", message = "Confirm password must contains at least 1 digit, 1 uppercase letter, 1 lowercase letter")
    private String confirmPassword;
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<NewUserRequest> {
        
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void doValidate(NewUserRequest target) {
            reject("email", () -> validateEmail(target.getEmail()));
            reject("password", () -> validatePassword(target.getPassword(), target.getConfirmPassword()));
        }
    
        private String validateEmail(final @Nullable String email) {
            if (!Objects.requireNonNull(email).contains("@")) {
                return "Invalid email";
            }
            if (userRepository.findOneByField("email", email).isPresent()) {
                return "Email already existed";
            }
            return null;
        }
    
        private String validatePassword(final String password, final String confirmPassword) {
            if (!Objects.equals(password, confirmPassword)) {
                return "Passwords are not matched";
            }
            return null;
        }
    }
}
