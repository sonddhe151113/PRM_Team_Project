package com.example.prm_team_project.models.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartItem {
    private Long productId;
    private long total;
    private String note;
    
    public void increase(long i) {
        total += i;
    }
}

