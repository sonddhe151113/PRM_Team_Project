package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldHaveLength;
import com.example.prm_team_project.annotations.input.ShouldMeetPattern;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.utils.ApiStrings;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@RequiresApi(api = Build.VERSION_CODES.N)
@ValidatedBy(ChangePasswordRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ChangePasswordRequest extends BaseRequest {
    private Long userId;
    
    @ShouldHaveLength(atLeast = 8, atMax = 32, message = "Current password must have 8-32 characters")
    @ShouldMeetPattern(value = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", message = "Current password must contains at least 1 digit, 1 uppercase letter, 1 lowercase letter")
    private String currentPassword;
    
    @ShouldHaveLength(atLeast = 8, atMax = 32, message = "New password must have 8-32 characters")
    @ShouldMeetPattern(value = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", message = "New password must contains at least 1 digit, 1 uppercase letter, 1 lowercase letter")
    private String newPassword;
    
    @ShouldHaveLength(atLeast = 8, atMax = 32, message = "Confirm password must have 8-32 characters")
    @ShouldMeetPattern(value = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", message = "Confirm password must contains at least 1 digit, 1 uppercase letter, 1 lowercase letter")
    private String confirmPassword;
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<ChangePasswordRequest> {
        
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void doValidate(ChangePasswordRequest target) {
            reject("currentPassword", () -> validateCurrentPassword(target.getUserId(), target.getCurrentPassword()));
            reject("password", () -> validateConfirmPassword(target.getNewPassword(), target.getConfirmPassword()));
        }
        
        private String validateCurrentPassword(final Long userId, final @Nullable String password) {
            User user = userRepository.findOneOrThrow(userId, "User not found");
            if (!ApiStrings.checkPassword(password, user.getPassword())) {
                return "Wrong password";
            }
            return null;
        }
        
        private String validateConfirmPassword(final String password, final String confirmPassword) {
            if (!Objects.equals(password, confirmPassword)) {
                return "Passwords are not matched";
            }
            return null;
        }
    }
}
