package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.annotations.input.ShouldNotNull;
import com.example.prm_team_project.entities.Customer;
import com.example.prm_team_project.utils.ApiConverters;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@RequiresApi(api = Build.VERSION_CODES.N)
@ValidatedBy(NewCustomerRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class NewCustomerRequest extends BaseAddRequest {
    
    @ShouldNotNull(message = "user id must not be null")
    private Long userId;
    
    private String address;
    
    private String contactTitle;
    
    private String contactName;
    
    public Customer toCustomer() {
        return ApiConverters.convertTo(Customer.class, this);
    }
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<NewCustomerRequest> {
        
        @Override
        protected void doValidate(NewCustomerRequest target) {
            reject("userId", () -> validateUserId(target.getUserId()));
        }
        
        private String validateUserId(final @Nullable Long userId) {
            if (userRepository.getOne(userId) == null) {
                return "userId not found";
            }
            return null;
        }
    }
}
