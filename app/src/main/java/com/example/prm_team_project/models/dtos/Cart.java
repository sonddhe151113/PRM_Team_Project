package com.example.prm_team_project.models.dtos;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.utils.ApiCollections;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@RequiresApi(api = Build.VERSION_CODES.N)
public class Cart {
    
    private final LinkedHashMap<Long, CartItem> items = Maps.newLinkedHashMap();
    
    public void add(Long productId, int total, String note) {
        if (items.containsKey(productId)) {
            CartItem cartItem = Objects.requireNonNull(items.get(productId));
            cartItem.increase(total);
            if (cartItem.getTotal() < 0) {
                items.remove(productId);
            }
            return;
        }
        if (total <= 0) {
            throw ApiException.withMessage("Total must be larger than 0");
        }
        items.put(productId, CartItem.builder().productId(productId).total(total).note(note).build());
    }
    
    public List<CartItem> getAll() {
        return ApiCollections.listOf(items.values());
    }
    
    public List<CartItem> getAll(List<Long> productIds) {
        Set<Long> ids = Sets.newHashSet(productIds);
        return items.entrySet().stream().filter(e -> ids.contains(e.getKey())).map(Map.Entry::getValue).collect(Collectors.toList());
    }
    
    private void add(Long productId, int total) {
        add(productId, total, null);
    }
    
    public void clear() {
        this.items.clear();
    }
    
    public void remove(Long productId) {
        this.items.remove(productId);
    }
    public void remove(List<Long> productIds) {
        ApiCollections.emptyListIfNull(productIds).forEach(items::remove);
    }
    
    public int countTotalItems() {
        return items.size();
    }
}
