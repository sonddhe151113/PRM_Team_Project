package com.example.prm_team_project.models.responses;

import com.example.prm_team_project.entities.Product;
import com.example.prm_team_project.utils.ApiConverters;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class CartDetailResponse {

    @Getter
    @Setter
    @AllArgsConstructor
    @SuperBuilder(toBuilder = true)
    public static class CartProductResponse extends Product {
        private String note;
        
        public static CartProductResponse of(Product p, String note, long total) {
            CartProductResponse response = ApiConverters.convertTo(CartProductResponse.class, p);
            response.setNote(note);
            response.setQuantity(total);
            return response;
        }
    }
    
    private List<CartProductResponse> items;
    private double totalPrice;
}
