package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.entities.Category;
import com.example.prm_team_project.utils.ApiConverters;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@RequiresApi(api = Build.VERSION_CODES.N)
@ValidatedBy(NewCategoryRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class NewCategoryRequest extends BaseAddRequest {
    
    @ShouldNotBlank(message = "name should not be blank")
    private String name;
    
    private String description;
    
    public Category toCategory() {
        return ApiConverters.convertTo(Category.class, this);
    }
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<NewCategoryRequest> {
        
        @Override
        protected void doValidate(NewCategoryRequest target) {
            reject("name", () -> validateName(target.getName()));
        }
    
        private String validateName(final String name) {
            if (categoryRepository.findOneByField("name", name).isPresent()) {
                return "Name already existed.";
            }
            return null;
        }
    }
}
