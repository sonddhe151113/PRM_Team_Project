package com.example.prm_team_project.models.requests;

import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.annotations.input.ShouldNotNull;
import com.example.prm_team_project.entities.User;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@RequiresApi(api = 33)
@ValidatedBy(NewAdminUserRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class NewAdminUserRequest extends BaseAddRequest {
    
    @ShouldNotBlank(message = "Please enter your username")
    private String username;
    
    @ShouldNotBlank(message = "Please enter your email")
    private String email;
    
    @ShouldNotNull(message = "Please select your role.")
    private User.Role role;
    
    public User toUser() {
        return User.builder().username(username).email(email).role(role).status(User.Status.ACTIVE).gender(User.Gender.MALE).build();
    }
    
    @Builder
    @Getter
    @NoArgsConstructor
    @RequiresApi(api = 33)
    public static class Validator extends BaseValidator<NewAdminUserRequest> {
        
        @Override
        protected void doValidate(NewAdminUserRequest target) {
            reject("email", () -> validateEmail(target.getEmail()));
        }
        
        private String validateEmail(final @Nullable String email) {
            if (!Objects.requireNonNull(email).contains("@")) {
                return "Invalid email";
            }
            if (userRepository.findOneByField("email", email).isPresent()) {
                return "Email already existed";
            }
            return null;
        }
    }
}
