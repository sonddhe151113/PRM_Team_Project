package com.example.prm_team_project.models.dtos;

public class SqlFailedException extends RuntimeException {
    public SqlFailedException(String message) {
        super(message);
    }
    
    public static SqlFailedException withMessage(String message) {
        return new SqlFailedException(message);
    }
}
