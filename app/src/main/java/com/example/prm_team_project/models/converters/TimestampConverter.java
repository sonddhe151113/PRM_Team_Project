package com.example.prm_team_project.models.converters;

import androidx.room.TypeConverter;

import java.sql.Timestamp;

public class TimestampConverter {
    @TypeConverter
    public static Timestamp toDate(Long milliseconds) {
        return milliseconds == null ? null : new Timestamp(milliseconds);
    }
    
    @TypeConverter
    public static Long toTimestamp(Timestamp date) {
        return date == null ? null : date.getTime();
    }
}
