package com.example.prm_team_project.models.requests;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.prm_team_project.annotations.ValidatedBy;
import com.example.prm_team_project.annotations.input.ShouldNotBlank;
import com.example.prm_team_project.annotations.input.ShouldNotNull;
import com.example.prm_team_project.entities.Customer;
import com.example.prm_team_project.utils.ApiConverters;
import com.example.prm_team_project.validators.BaseValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;


@ValidatedBy(UpdateCustomerRequest.Validator.class)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class UpdateCustomerRequest extends BaseUpdateRequest {
    
    @ShouldNotNull(message = "customer id must not be null")
    private Long customerId;
    
    @ShouldNotNull(message = "user id must not be null")
    private Long userId;
    
    @ShouldNotBlank(message = "address should not be blank")
    private String address;
    
    @ShouldNotBlank(message = "contact title should not be blank")
    private String contactTitle;
    
    @ShouldNotBlank(message = "contact name should not be blank")
    private String contactName;
    
    @RequiresApi(api = 33)
    public Customer updateFor(Customer customer) {
        return ApiConverters.updateFor(customer, this);
    }
    
    @Builder
    @Getter
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static class Validator extends BaseValidator<UpdateCustomerRequest> {
        
        @Override
        protected void doValidate(UpdateCustomerRequest target) {
            reject("userId", () -> validateUserId(target.getUserId()));
        }
        
        private String validateUserId(final @Nullable Long userId) {
            if (userRepository.getOne(userId) == null) {
                return "userId not found";
            }
            return null;
        }
    }
}
